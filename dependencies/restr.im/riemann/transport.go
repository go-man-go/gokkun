package riemann

import (
	"bufio"
	"bytes"
	"log"
	"net"
	"time"
)

var maxbuffersize = 2048

type Transport struct {
	w             *bufio.Writer
	e             chan []byte
	Addr          string
	FlushInterval time.Duration
	tick          <-chan time.Time
	con           net.Conn
}

func NewTransport(addr string, interval time.Duration) (*Transport, error) {
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return nil, err
	}
	w := bufio.NewWriterSize(conn, maxbuffersize)
	riemann_transport := Transport{
		w:             w,
		e:             make(chan []byte, 10000),
		Addr:          addr,
		tick:          time.Tick(interval),
		FlushInterval: interval,
	}
	go riemann_transport.ReceiveAndTick()
	return &riemann_transport, nil
}

func (g *Transport) ReceiveAndTick() {

	for {
		select {
		case <-g.tick:
			err := g.w.Flush()
			if err != nil {
				log.Println("Error flushing to riemann: ", err)
			}

		case data := <-g.e:
			count, err := g.w.Write(data)
			// reconnect
			if err != nil {
				log.Printf("Failed to send data to %v size: %v, error: %v\n", g.Addr, count, err)
				for {
					conn, err := net.Dial("tcp", g.Addr)
					if err != nil {
						log.Printf("Error reconnecting to %v, error: %v\n", g.Addr, err)
						time.Sleep(time.Second)
						continue
					}
					g.w = bufio.NewWriterSize(conn, maxbuffersize)
					break
				}
			}
		}
	}

}

func (r *Transport) Send(msg *Msg) {
	if msg == nil {
		return
	}
//	log.Printf("Send message: %v\n", msg)
	buf := &bytes.Buffer{}
	enc := NewEncoder(buf)
	enc.EncodeMsg(msg)
	r.e <- buf.Bytes()
}
