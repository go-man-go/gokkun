package riemann

func (m *Event) SetState(state string) {
	m.State = &state
}

func (m *Event) SetService(service string) {
	m.Service = &service
}

func (m *Event) SetDescription(description string) {
	m.Description = &description
}

func (m *Event) SetMetric(metric float64) {
	metric = float64(int(metric*100)) / 100
	m.MetricD = &metric
}

func (m *Event) SetTags(tags []string) {
	m.Tags = tags
}

func (m *Event) SetTsdbService(service string) {
	m.TsdbService = &service
}

func (m *Event) SetTtl(ttl float32) {
	m.Ttl = &ttl
}

func (m *Msg) AddEvent(e *Event) {
	m.Events = append(m.Events, e)
}
