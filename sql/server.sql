create sequence servers_id_seq;

create table servers
(
	id integer not null primary key default nextval('public.servers_id_seq'::text),
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	dtime timestamp not null default now(),
	status smallint not null default 0,
	fqdn text,
	data text
);
create index servers_fqdn on servers (fqdn);
