--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: intarray; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS intarray WITH SCHEMA public;


--
-- Name: EXTENSION intarray; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION intarray IS 'functions, operators, and index support for 1-D arrays of integers';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: documents; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE documents (
    id integer DEFAULT nextval(('public.documents_id_seq'::text)::regclass) NOT NULL,
    class text NOT NULL,
    ctime timestamp without time zone DEFAULT now() NOT NULL,
    mtime timestamp without time zone DEFAULT now() NOT NULL,
    dtime timestamp without time zone DEFAULT now() NOT NULL,
    status smallint DEFAULT 0 NOT NULL,
    sections integer[],
    name text,
    data text
);


--
-- Name: documents_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: links; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE links (
    id integer DEFAULT nextval(('public.documents_id_seq'::text)::regclass) NOT NULL,
    class text NOT NULL,
    ctime timestamp without time zone DEFAULT now() NOT NULL,
    mtime timestamp without time zone DEFAULT now() NOT NULL,
    status smallint DEFAULT 1 NOT NULL,
    source_id integer NOT NULL,
    source_class text NOT NULL,
    dest_id integer NOT NULL,
    dest_class text NOT NULL,
    data text
);


--
-- Name: options; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE options (
    id integer DEFAULT nextval(('public.documents_id_seq'::text)::regclass) NOT NULL,
    pid integer,
    name text,
    value text,
    type text NOT NULL
);


--
-- Name: sections; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE sections (
    id integer DEFAULT nextval(('public.documents_id_seq'::text)::regclass) NOT NULL,
    pid integer,
    class text NOT NULL,
    ctime timestamp without time zone DEFAULT now() NOT NULL,
    mtime timestamp without time zone DEFAULT now() NOT NULL,
    status smallint DEFAULT 0 NOT NULL,
    sorder integer DEFAULT 1,
    name text,
    alias text,
    data text
);



--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer DEFAULT nextval(('public.documents_id_seq'::text)::regclass) NOT NULL,
    login text NOT NULL,
    class text NOT NULL,
    ctime timestamp without time zone DEFAULT now() NOT NULL,
    mtime timestamp without time zone DEFAULT now() NOT NULL,
    status smallint DEFAULT 1 NOT NULL,
    name text,
    passwd text NOT NULL,
    groups integer[],
    data text
);


--
-- Data for Name: documents; Type: TABLE DATA; Schema: public; Owner: -
--

COPY documents (id, class, ctime, mtime, dtime, status, sections, name, data) FROM stdin;
\.


--
-- Name: documents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('documents_id_seq', 201, true);


--
-- Data for Name: links; Type: TABLE DATA; Schema: public; Owner: -
--

COPY links (id, class, ctime, mtime, status, source_id, source_class, dest_id, dest_class, data) FROM stdin;
\.


--
-- Data for Name: options; Type: TABLE DATA; Schema: public; Owner: -
--

COPY options (id, pid, name, value, type) FROM stdin;
167	\N	s_alias	\N	HASH
168	167	sources	132	SCALAR
169	167	servers	166	SCALAR
170	\N	tabs	\N	HASH
171	170	rubricator	\N	HASH
172	171	lefts	\N	ARRAY
173	172	0	finder	SCALAR
174	171	name	Тематический рубрикатор	SCALAR
175	171	id	rubricator	SCALAR
176	171	level	3	SCALAR
177	171	sections	\N	ARRAY
178	177	0	Contenido::Section	SCALAR
179	170	admin	\N	HASH
180	179	sections	\N	ARRAY
181	180	0	Contenido::Section::DefaultSection	SCALAR
182	179	id	admin	SCALAR
183	179	name	Администрирование	SCALAR
184	179	lefts	\N	ARRAY
185	184	0	structure	SCALAR
186	184	1	project	SCALAR
187	\N	documents	\N	HASH
188	187	gokkun::Source	\N	HASH
189	188	alias	\N	ARRAY
190	189	0	files	SCALAR
191	189	1	postgres	SCALAR
192	187	Contenido::Document::DefaultDocument	\N	HASH
193	\N	colors	\N	HASH
194	\N	widths	\N	HASH
195	\N	sections	\N	HASH
196	195	Contenido::Section::DefaultSection	\N	HASH
197	\N	redirects	\N	HASH
198	\N	links	\N	HASH
199	198	Contenido::Link::DefaultLink	\N	HASH
200	\N	users	\N	HASH
201	200	Contenido::User::DefaultUser	\N	HASH
\.


--
-- Data for Name: sections; Type: TABLE DATA; Schema: public; Owner: -
--

COPY sections (id, pid, class, ctime, mtime, status, sorder, name, alias, data) FROM stdin;
1	\N	Contenido::Section	2018-01-29 15:22:23.484127	2018-01-29 15:22:23.484127	1	1	Корневая секция	\N	\N
132	1	Contenido::Section	2018-01-29 16:51:46.018313	2018-01-29 16:51:46.018313	1	2	Sources	sources	{"filters":"","_sorted_order":"","default_table_class":"","order_by":"","default_document_class":"gokkun::Source"}
166	1	Contenido::Section	2018-04-03 15:14:34.85313	2018-04-03 15:14:34.85313	1	3	Servers	servers	{"order_by":"","filters":"","default_table_class":"","default_document_class":"gokkun::Server","_sorted_order":""}
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY users (id, login, class, ctime, mtime, status, name, passwd, groups, data) FROM stdin;
102	ahitrov	Contenido::User	2018-01-29 15:25:29.989889	2018-01-29 15:25:29.989889	1	Andrew Hitrov	Blacksilver	{1}	\N
\.


--
-- Name: documents_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY documents
    ADD CONSTRAINT documents_pkey PRIMARY KEY (id);


--
-- Name: links_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY links
    ADD CONSTRAINT links_pkey PRIMARY KEY (id);


--
-- Name: options_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY options
    ADD CONSTRAINT options_pkey PRIMARY KEY (id);


--
-- Name: sections_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY sections
    ADD CONSTRAINT sections_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: documents_dtime; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX documents_dtime ON documents USING btree (dtime);


--
-- Name: documents_sections; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX documents_sections ON documents USING gist (sections);


--
-- Name: links_dest; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX links_dest ON links USING btree (dest_id);


--
-- Name: links_source; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX links_source ON links USING btree (source_id);


--
-- Name: sections_alias_pid; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX sections_alias_pid ON sections USING btree (alias, pid);


--
-- Name: sections_parent; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX sections_parent ON sections USING btree (pid);


--
-- Name: users_login; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX users_login ON users USING btree (login);


--
-- PostgreSQL database dump complete
--

