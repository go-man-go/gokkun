create sequence sources_id_seq;

create table sources
(
	id integer not null primary key default nextval('public.sources_id_seq'::text),
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	dtime timestamp not null default now(),
	status smallint not null default 0,
	master_id integer,
	fqdn text,
	alias text,
	cron text,
	env text,
	ip text,
	keep_remote integer,
	timeout_exec integer,
	timeout_sync integer,
	tags text,
	data text
);
create index sources_master_id on sources (master_id);
create index sources_fqdn on sources (fqdn);
create unique index sources_fqdn_job on sources (fqdn, alias, cron);
