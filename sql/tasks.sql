create sequence tasks_id_seq;

create table tasks
(
	id integer not null primary key default nextval('public.tasks_id_seq'::text),
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	start timestamp,
	finish timestamp,
	status smallint not null default 0,
	master_id integer,
	source_id integer,
	pid integer,
	server_dir text,
	client_dir text,
	env text,
	message text,
	data text
);
create index tasks_master_id on tasks (master_id);
create index tasks_source_id on tasks (source_id);
