var isLoading = false;
var taskTimerId;
var nReloadInterval = 0;
function reloadTasks( sType, nSourceID, nPage ) {
	var Data = {}
	if ( sType ) {
		Data.status = sType;
	}
	if ( parseInt(nSourceID) > 0 ) {
		Data.source_id = nSourceID;
	}
	if ( parseInt(nPage) > 1 ) {
		Data.page = page;
	}
	if ( !isLoading ) {
		isLoading = true;
		$('#autoreload-prompt').html('loading...');
		$.ajax({
			url	: '/ajax/tasks',
			data	: Data,
			dataType	: 'json',
			success	: function( result ) {
				var oParent = $('#task-list');
				var sHTML = "";
				for ( i = 0; i < result.Tasks.length; i++ ) {
					var row = result.Tasks[i];
					sHTML += '<tr><td><a href="/output_task/'+ row.ID +'">'+ row.ID +'</a></td><td><a href="/sources/'+ row.SourceID +'">'+ row.Fqdn +'</a></td><td>'+ row.Alias +'</td><td>'+ row.StartStr +'</td><td>'+ row.FinishStr +'</td><td><a href="/output_task/'+ row.ID +'">'+ row.StatusName +'</a></td>';
					if ( row.Status == 0 || nSourceID > 0 ) {
						sHTML += '<td>'+ row.Message +'</td>';
					}
					sHTML += '</tr>';
				}
				oParent.html(sHTML);
				isLoading = false;
				$('#autoreload-prompt').html('');
			},
			error	: function( jqXHR, textStatus ) {
				isLoading = false;
				$('#autoreload-prompt').html('');
			}
		});
	}
}

// Takes interval in seconds
function startTaskRobot( nInterval ) {
	var sType = globalType;
	var nSourceID = globalSourceID || 0;
	var nPage = globalPageNum || 1;
	if ( taskTimerId ) {
		clearInterval(taskTimerId);
	}
	if ( nInterval && nInterval > 0 ) {
		nInterval *= 1000;
		nReloadInterval = nInterval;
		taskTimerId = setInterval(function() { reloadTasks(sType, nSourceID, nPage); }, nInterval);
	} else if ( nReloadInterval > 0 ) {
		taskTimerId = setInterval(function() { reloadTasks(sType, nSourceID, nPage); }, nInterval);
	} else {
		stopTaskRobot();
	}
}

function stopTaskRobot() {
	if ( taskTimerId ) {
		clearInterval(taskTimerId);
		alert(taskTimerId);
	}
}