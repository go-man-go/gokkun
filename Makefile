export GOPATH := $(CURDIR)/.build
GOPKG := gokkun
TMPSRC := $(GOPATH)/src

all: build

build: clean
	mkdir -p $(GOPATH)/src $(GOPATH)/bin $(GOPATH)/pkg
	mkdir -p $(TMPSRC)
	cp -av $(GOPKG) $(TMPSRC)
	cp -av dependencies/github.com $(TMPSRC)
	cp -av dependencies/restr.im $(TMPSRC)
	cd $(TMPSRC)/$(GOPKG) && GOPATH=$(GOPATH) go build -x -v $(GOPKG)
	mkdir -p $(CURDIR)/prebuild
	cp $(TMPSRC)/$(GOPKG)/$(GOPKG) $(CURDIR)/prebuild/gokkun
	chmod 0755 $(CURDIR)/prebuild/gokkun

clean:
	rm -rf $(CURDIR)/prebuild
	rm -rf $(GOPATH)

install:
	mkdir -p $(DESTDIR)/usr/bin
	cp -a $(CURDIR)/prebuild/gokkun $(DESTDIR)/usr/bin
	chmod 0755 $(DESTDIR)/usr/bin/gokkun
	mkdir -p $(DESTDIR)/usr/share
	@if [ -d $(DESTDIR)/usr/share/gokkun-face ]; then			\
		rm -Rf $(DESTDIR)/usr/share/gokkun-face;				\
	fi;
	cp -a -r $(CURDIR)/gokkun-face $(DESTDIR)/usr/share/
#	mkdir -p $(DESTDIR)/etc
#	cp -a $(CURDIR)/etc/gokkun.toml $(DESTDIR)/etc

.PHONY: all clean install
