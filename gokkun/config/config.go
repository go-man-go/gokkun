package config

import (
	"io"
	"log"
	"net"
	"net/http"
	"runtime"
	"sync"
	"time"

	"github.com/BurntSushi/toml"
	"restr.im/riemann"
)

var netTransport = &http.Transport{
	Dial: (&net.Dialer{
		Timeout: 10 * time.Second,
	}).Dial,
	TLSHandshakeTimeout: 10 * time.Second,
}

var (
	DEBUG          bool
	FQDN           string
	TAGS           []string
	Riemann        *riemann.Transport
	Mu             sync.RWMutex
	CircuitBreaker bool
)

type RiemannConfig struct {
	Address string   `toml:"address"`
	Tags    []string `toml:"tags"`
}

type PostgresConfig struct {
	Host     string `toml:"host"`
	Port     string `toml:"port"`
	User     string `toml:"user"`
	Password string
	Database string `toml:"database"`
}

type ApiConfig struct {
	Host string `toml:"host"`
	Port string `toml:"port"`
}

type MonitorConfig struct {
	Host         string `toml:"host"`
	Port         string `toml:"port"`
	DocumentRoot string `toml:"document_root"`
}

type ClientConfig struct {
	BaseDir      string `toml:"basedir"`
	TemplateRoot string `toml:"template_root"`
}

type ServerConfig struct {
	BaseDir string `toml:"basedir"`
	JobNum  int    `toml:"jobs"`
}

type Conf struct {
	Postgres    PostgresConfig `toml:"postgres"`
	Api         ApiConfig      `toml:"api"`
	Monitor     MonitorConfig  `toml:"monitor"`
	Server      ServerConfig   `toml:"server"`
	Client      ClientConfig   `toml:"client"`
	Riemann     RiemannConfig  `toml:"riemann"`
	ChefFaceUrl string         `toml:"chef_face_url"`
}

type Event struct {
	State       string
	Service     string
	Description string
	Metric      float64
	Warning     float64
	Critical    float64
	Tags        []string
	TsdbService string
	Ttl         float32
}

var (
	PGPass = "floitHimJur9"
)

func NewConfig(file string) (*Conf, error) {
	config := &Conf{
		ChefFaceUrl: "http://chef-face.restr.im/gokkun.json",
	}
	_, err := toml.DecodeFile(file, config)
	if err != nil {
		return nil, err
	}
	if len(config.Riemann.Tags) == 0 {
		config.Riemann.Tags = []string{"gokkun"}
	}
	TAGS = config.Riemann.Tags
	if config.Client.BaseDir == "" {
		config.Client.BaseDir = "/storage/backups/"
	} else if config.Client.BaseDir[len(config.Client.BaseDir)-1] == '/' {
		config.Client.BaseDir = config.Client.BaseDir[0:len(config.Client.BaseDir)-1]
	}
	if config.Server.BaseDir == "" {
		config.Server.BaseDir = "/restream-backup/"
	} else if config.Server.BaseDir[len(config.Server.BaseDir)-1] == '/' {
		config.Server.BaseDir = config.Server.BaseDir[0:len(config.Server.BaseDir)-1]
	}
	if config.Server.JobNum == 0 {
		config.Server.JobNum = runtime.NumCPU()
	}
	if config.Monitor.Port == "" {
		config.Monitor.Port = "8181"
	}
	if config.Monitor.DocumentRoot == "" {
		config.Monitor.DocumentRoot = "./"
	} else if config.Monitor.DocumentRoot[len(config.Monitor.DocumentRoot)-1] != '/' {
		config.Monitor.DocumentRoot += "/"
	}
	if config.Client.TemplateRoot == "" {
		config.Client.TemplateRoot = config.Monitor.DocumentRoot
	} else if config.Client.TemplateRoot[len(config.Client.TemplateRoot)-1] != '/' {
		config.Client.TemplateRoot += "/"
	}
	if config.Api.Port == "" {
		config.Api.Port = "8080"
	}
	config.Postgres.Password = PGPass
	return config, nil
}

func HttpGet(url string) (*http.Response, error) {
	var httpClient = &http.Client{
		Timeout: time.Second * 60,
	}
	resp, err := httpClient.Get(url)
	return resp, err
}

func HttpPost(url string, contentType string, body io.Reader) (*http.Response, error) {
	var httpClient = &http.Client{
		Timeout: time.Second * 60,
	}
	resp, err := httpClient.Post(url, contentType, body)
	return resp, err
}

func HttpDelete(url string) (*http.Response, error) {
	var httpClient = &http.Client{
		Timeout: time.Second * 60,
	}

	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := httpClient.Do(req)
	return resp, err
}

func newEvent() *riemann.Event {
	now := time.Now().Unix()
	host := FQDN
	event := &riemann.Event{
		Time: &now,
		Host: &host,
		Tags: TAGS,
	}
	return event
}

func NewMsg() *riemann.Msg {
	msg := &riemann.Msg{}
	return msg
}

func (e *Event) calculateWarningCriticalState() {
	if e.State != "" {
		return
	}
	if e.Warning == e.Critical && e.Critical < e.Warning {
		return
	}
	e.State = "ok"
	if e.Metric > e.Warning && e.Warning != 0.0 {
		e.State = "warning"
	}
	if e.Metric > e.Critical && e.Critical != 0.0 {
		e.State = "critical"
	}
	return
}

func (e *Event) ToRiemannEvent() *riemann.Event {
	event := newEvent()
	e.calculateWarningCriticalState()
	event.SetState(e.State)
	event.SetService(e.Service)
	event.SetDescription(e.Description)
	event.SetMetric(e.Metric)
	if len(e.Tags) != 0 {
		event.SetTags(e.Tags)
	}
	if e.TsdbService != "" {
		event.SetTsdbService(e.TsdbService)
	}
	if e.Ttl != 0.0 {
		event.SetTtl(e.Ttl)
	}
	return event
}

func CryAndDie(Source, Message string) {
	// Report to Riemann
	msg := NewMsg()
	event := Event{
		Service:     Source + ": " + FQDN,
		State:       "critical",
		Description: Message,
	}
	msg.AddEvent(event.ToRiemannEvent())
	Riemann.Send(msg)
	log.Fatalf("[%s] %s", Source, Message)
}

func BreakExecution() {
	Mu.Lock()
	defer Mu.Unlock()
	CircuitBreaker = false
}

func CanExecute() bool {
	Mu.RLock()
	defer Mu.RUnlock()
	if DEBUG && !CircuitBreaker {
		log.Printf("Execution must stop")
	}
	return CircuitBreaker
}
