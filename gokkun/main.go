package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"restr.im/riemann"

	"gokkun/api"
	"gokkun/client"
	"gokkun/config"
	"gokkun/ginit"
	"gokkun/keeper"
	"gokkun/monitor"
	"gokkun/server"
)

var (
	configPath = flag.String("config", filepath.FromSlash("/etc/gokkun.toml"), "Path to config file")
	modeUsage  = flag.String("mode", "test", "Gokkun functioning mode. Default is 'test'")
	setDebug   = flag.Bool("debug", false, "Switch on Gokkun debugging messages")
)

func main() {

	//	runtime.GOMAXPROCS(runtime.NumCPU() * 4)
	flag.Parse()

	cmd := exec.Command("/bin/hostname", "-f")
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Fatalf("Getting hostname error: %v", err)
	}
	fqdn := out.String()
	fqdn = fqdn[:len(fqdn)-1]
	config.FQDN = fqdn
	fmt.Println(fqdn)

	if *modeUsage == "test" {
		fmt.Println("Test mode")
	} else {
		fmt.Printf("Gokkun is in %s mode\n", *modeUsage)
	}
	config.CircuitBreaker = true

	c, err := config.NewConfig(*configPath)
	if err != nil {
		fmt.Printf("Error in config: %v\n", err)
		os.Exit(1)
	}
	if *setDebug {
		fmt.Println("Debug mode switched on")
		config.DEBUG = true
	}

	fmt.Printf("Config: %v\n", c)

	r, err := riemann.NewTransport(c.Riemann.Address, time.Second)
	if err != nil {
		fmt.Printf("Error initialize connect with riemann server: %v\n", err)
		os.Exit(2)
	}
	config.Riemann = r

	// перехватываем Ctr+C
	CtrlC := make(chan os.Signal, 1)
	signal.Notify(CtrlC, os.Interrupt)
	signal.Notify(CtrlC, syscall.SIGTERM)
	go func() {
		<-CtrlC
		if *modeUsage == "server" {
			config.BreakExecution()
		} else {
			os.Exit(1)
		}
	}()

	switch *modeUsage {
	case "test":
		//			source := keeper.Source{Fqdn: "backup-staging.virt.restr.im", Status: 0, Luptime: time.Now()}
		//			Keeper.DB.Create(&source)
	case "chef":
		Keeper, err := keeper.New(c)
		if err == nil {
			defer Keeper.DB.Close()
		} else {
			fmt.Printf("%v\n", err)
			os.Exit(1)
		}
		fmt.Printf("Connection: %v\n", Keeper)
		if config.DEBUG {
			ginit.InitFromChef(Keeper, c)
		} else {
			ginit.Start(Keeper, c)
		}
	case "api":
		Keeper, err := keeper.New(c)
		if err == nil {
			defer Keeper.DB.Close()
		} else {
			fmt.Printf("%v\n", err)
			os.Exit(1)
		}
		fmt.Printf("Connection: %v\n", Keeper)
		api.NewApi(Keeper, c)
	case "mon":
		Keeper, err := keeper.New(c)
		if err == nil {
			defer Keeper.DB.Close()
		} else {
			fmt.Printf("%v\n", err)
			os.Exit(1)
		}
		fmt.Printf("Connection: %v\n", Keeper)
		monitor.NewMon(Keeper, c)
	case "client":
		client := client.New(c, fqdn)
		client.Start()
	case "server":
		server := server.New(c, fqdn)
		server.Start()
	default:
		fmt.Printf("Unknown mode %s. Exiting\n", *modeUsage)
		os.Exit(1)
	}

}
