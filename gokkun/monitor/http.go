package monitor

import (
	//	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	"gokkun/config"
	"gokkun/keeper"
)

const RowLimit int = 20

type Mon struct {
	Keeper        *keeper.Keeper
	Config        *config.Conf
	Template      *template.Template
	StaticHandler http.Handler
}

type Href struct {
	Url, Class, Alt, Text string
}

func NewMon(k *keeper.Keeper, c *config.Conf) {
	m := Mon{
		Keeper: k,
		Config: c,
	}
	//	http.HandleFunc("/", m.Home)
	staticRoot := filepath.Join(c.Monitor.DocumentRoot, "gokkun-face", "static")
	log.Printf("Root for static: %s\n", staticRoot)
	m.StaticHandler = http.FileServer(http.Dir(staticRoot))
	m.Template = template.Must(template.ParseGlob(filepath.Join(c.Monitor.DocumentRoot, "gokkun-face", "tmpl", "*")))

	err := http.ListenAndServe(c.Monitor.Host+":"+c.Monitor.Port, &m)
	if err != nil {
		log.Fatal("HTTP-monitor error: %v\n", err)
	}
}

func (m *Mon) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Printf("Path: %v\n", r.URL.Path)
	log.Printf("Scheme: %v\n", r.URL.Scheme)
	log.Printf("Method: %v\n", r.Method)
	log.Printf("Query: %v", r.URL.Query())

	if r.URL.Path == "/" {
		m.Home(w, r)
	} else {
		var base string
		base, r.URL.Path = shiftPath(r.URL.Path)
		log.Printf("ServeHTTP: url base = [%s]\n", base)
		switch base {
		case "ajax":
			m.AjaxHandler(w, r)
		case "sources":
			var idstr string
			idstr, r.URL.Path = shiftPath(r.URL.Path)
			if idstr == "" {
				m.Sources(w, r)
				return
			} else {
				if id, err := strconv.ParseInt(idstr, 10, 64); err != nil {
					SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
				} else {
					var suffix string
					suffix, r.URL.Path = shiftPath(r.URL.Path)
					switch suffix {
					case "":
						m.Source(w, r, id)
						return
					case "script":
						m.GenScript(w, r, id)
						return
					default:
						SendHTTPResponse(w, "Not Found", http.StatusNotFound)
					}
				}
			}
		case "search":
			m.Search(w, r)
			return
		case "failed", "executing", "success", "copying", "scheduled":
			m.Tasks(w, r, base)
			return
		case "output_task":
			var idstr string
			idstr, r.URL.Path = shiftPath(r.URL.Path)
			if idstr == "" {
				SendHTTPResponse(w, "Не указан ID", http.StatusInternalServerError)
				return
			} else {
				if id, err := strconv.ParseInt(idstr, 10, 64); err != nil {
					SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
				} else {
					m.Task(w, r, id)
					return
				}
			}
		case "static":
			m.StaticHandler.ServeHTTP(w, r)
			return
		default:
			SendHTTPResponse(w, "Not Found", http.StatusNotFound)
		}
	}
}

func (m *Mon) AjaxHandler(w http.ResponseWriter, r *http.Request) {
	var action string
	action, r.URL.Path = shiftPath(r.URL.Path)
	switch action {
	case "tasks":
		m.AjaxTasks(w, r)
		return
	default:
		SendHTTPResponse(w, "Not Found", http.StatusNotFound)
	}
}

type HTMLayout struct {
	Title   string
	Header  string
	Base    string
	Pattern string
}

type HomeResponse struct {
	HTMLayout
	Status int
	Tasks  []keeper.Task
}

func (m *Mon) Home(w http.ResponseWriter, r *http.Request) {
	data := HomeResponse{
		HTMLayout: HTMLayout{Title: "Hello!"},
	}
	log.Println("Template list:")
	for _, t := range m.Template.Templates() {
		log.Println(t.Name())
	}
	t := m.Template.Lookup("home")
	log.Printf("Found 'home': %v\n", t.Name())
	if t != nil {
		t.Execute(w, data)
	} else {
		SendHTTPResponse(w, "Template 'home' not found", 400)
	}
}

func shiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}

type PaginatorArgs struct {
	Total     int
	ElemCount int
	PageSize  int
	PageNum   int
	Base      string
	PName     string
	Params    map[string]string
}

func Paginator(a *PaginatorArgs) (items []*Href) {
	if a.PName == "" {
		a.PName = "page"
	}
	if a.ElemCount == 0 {
		a.ElemCount = 8
	}

	params := []string{}
	if len(a.Params) > 0 {
		for key, value := range a.Params {
			if key == a.PName {
				continue
			}
			params = append(params, key+"="+url.QueryEscape(value))
		}
	}

	parts := a.Total / a.PageSize
	if a.Total%a.PageSize > 0 {
		parts += 1
	}
	if parts <= 1 {
		items = append(items, &Href{Text: "1"})
		return items
	}
	mid_position := a.ElemCount / 2

	left_pushed_border := mid_position

	var left_border int
	var right_border int

	if a.PageNum >= left_pushed_border {
		left_border = a.PageNum - mid_position + 1
		right_border = left_border + a.ElemCount - 1
	} else {
		left_border = 1
		right_border = a.ElemCount
	}
	if right_border > parts {
		left_border = left_border - (right_border - parts)
		right_border = parts
	}
	if left_border < 1 {
		left_border = 1
	}
	if a.PageNum > 1 {
		pairs := params
		if a.PageNum > 2 {
			pairs = append(pairs, a.PName+"="+strconv.Itoa(a.PageNum-1))
		}
		url := a.Base
		if len(pairs) > 0 {
			url += "?" + strings.Join(pairs, "&")
		}
		items = append(items, &Href{Url: url, Class: "prev", Alt: "Предыдущая", Text: "Предыдущая"})
	} else {
		items = append(items, &Href{Class: "prev", Alt: "Предыдущая", Text: "Предыдущая"})
	}
	if left_border > 1 {
		url := a.Base
		if len(params) > 0 {
			url += "?" + strings.Join(params, "&")
		}
		items = append(items, &Href{Url: url, Alt: "Страница 1", Text: "1"})
		items = append(items, &Href{Text: "..."})
	}
	for pn := left_border; pn <= right_border; pn++ {
		pnum := strconv.Itoa(pn)
		href := Href{Text: pnum, Alt: "Страница " + pnum}
		if pn == a.PageNum {
			href.Class = "current"
		} else if pn > 1 {
			pairs := params
			pairs = append(pairs, a.PName+"="+pnum)
			href.Url = a.Base + "?" + strings.Join(pairs, "&")
		} else {
			if len(params) > 0 {
				href.Url = a.Base + "?" + strings.Join(params, "&")
			} else {
				href.Url = a.Base
			}
		}
		items = append(items, &href)
	}
	if right_border < parts {
		pnum := strconv.Itoa(parts)
		pairs := params
		pairs = append(pairs, a.PName+"="+pnum)
		url := a.Base + "?" + strings.Join(pairs, "&")
		items = append(items, &Href{Text: "..."})
		items = append(items, &Href{Url: url, Alt: "Страница " + pnum, Text: pnum})
	}
	if a.PageNum < parts {
		pairs := params
		pairs = append(pairs, a.PName+"="+strconv.Itoa(a.PageNum+1))
		url := a.Base + "?" + strings.Join(pairs, "&")
		items = append(items, &Href{Url: url, Class: "next", Alt: "Следующая", Text: "Следующая"})
	} else {
		items = append(items, &Href{Class: "next", Alt: "Следующая", Text: "Следующая"})
	}
	return items
}

var LF = []byte{0xA}

func SendHTTPResponse(w http.ResponseWriter, msg string, status int) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(status)

	w.Write([]byte(msg))
	w.Write(LF)
}

func SendJSONResponse(w http.ResponseWriter, msg []byte) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)

	w.Write(msg)
	w.Write(LF)
}

func SendTextResponse(w http.ResponseWriter, msg string) {
	w.Header().Set("Content-Type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)

	w.Write([]byte(msg))
	w.Write(LF)
}
