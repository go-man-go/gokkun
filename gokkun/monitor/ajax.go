package monitor

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

type TasksJSONResponse struct {
	HTTPStatus   int
	Result       string
	Message      string
	Tasks        []*TaskTable
	RowsTotal    int
	RowsRead     int
	UsePaginator bool
	Paginator    []*Href
}

func (m *Mon) AjaxTasks(w http.ResponseWriter, r *http.Request) {
	result := TasksJSONResponse{}
	wheres := []string{}
	values := []interface{}{}

	q := r.URL.Query()
	status := q.Get("status")
	if status != "" {
		switch status {
		case "failed", "executing", "success", "copying", "scheduled":
			wheres = append(wheres, "tasks.status = ?")
			values = append(values, TaskResponseMap[status].Status)
		default:
			result.HTTPStatus = http.StatusInternalServerError
			result.Result = "Error"
			result.Message = "Unknown task type"
		}
	}

	page := 1
	if result.HTTPStatus == 0 {
		pagestr := q.Get("page")
		if pagestr != "" {
			if i, err := strconv.Atoi(pagestr); err != nil {
				result.HTTPStatus = http.StatusInternalServerError
				result.Result = "Error"
				result.Message = err.Error()
			} else {
				page = i
			}
		}
	}
	if result.HTTPStatus == 0 {
		source_id := q.Get("source_id")
		if source_id != "" {
			if sid, err := strconv.Atoi(source_id); err != nil {
				result.HTTPStatus = http.StatusInternalServerError
				result.Result = "Error"
				result.Message = err.Error()
			} else {
				wheres = append(wheres, "tasks.source_id = ?")
				values = append(values, sid)
			}
		}
	}

	tasks := []*TaskTable{}
	if result.HTTPStatus == 0 {
		if err := m.Keeper.DB.Table("tasks").Select("tasks.ID, tasks.ctime, tasks.mtime, tasks.source_id, tasks.start, tasks.finish, sources.alias, sources.fqdn, tasks.status, tasks.server_dir, tasks.client_dir, tasks.message").Joins("join sources on tasks.source_id = sources.id").Where(strings.Join(wheres, " AND "), values...).Order("tasks.ctime desc").Limit(RowLimit).Offset((page - 1) * RowLimit).Find(&tasks).Error; err != nil {
			result.HTTPStatus = http.StatusInternalServerError
			result.Result = "Error"
			result.Message = err.Error()
		}
	}
	var taskCount int
	if result.HTTPStatus == 0 {
		if err := m.Keeper.DB.Table("tasks").Joins("join sources on tasks.source_id = sources.id").Where(strings.Join(wheres, " AND "), values...).Count(&taskCount).Error; err != nil {
			result.HTTPStatus = http.StatusInternalServerError
			result.Result = "Error"
			result.Message = err.Error()
		}
	}

	if result.HTTPStatus == 0 {
		for _, task := range tasks {
			task.StartStr = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", task.Start.Year(), task.Start.Month(), task.Start.Day(), task.Start.Hour(), task.Start.Minute(), task.Start.Second())
			task.FinishStr = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", task.Finish.Year(), task.Finish.Month(), task.Finish.Day(), task.Finish.Hour(), task.Finish.Minute(), task.Finish.Second())
			task.StatusName = TaskStatusMap[task.Status].Name
		}
		result.Tasks = tasks
		result.RowsRead = len(tasks)
		result.RowsTotal = taskCount
		result.HTTPStatus = http.StatusOK
		result.Result = "OK"
	}

	body, _ := json.Marshal(result)
	SendJSONResponse(w, body)
}
