package monitor

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gokkun/keeper"
)

type SearchResponse struct {
	HTMLayout
	Sources      []*SourceTable
	SourcesFound int
	Tasks        []*TaskTable
	TasksFound   int
	TasksRead    int
	Page         int
	UsePaginator bool
	Paginator    []*Href
}

func (m *Mon) Search(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()
	pagestr := q.Get("page")
	page := 1
	pageparams := map[string]string{}
	if pagestr != "" {
		if i, err := strconv.Atoi(pagestr); err != nil {
			SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
			return
		} else {
			page = i
		}
	}

	statuses := map[int]int{}
	statusparam := q.Get("status")
	if statusparam != "" {
		pageparams["status"] = statusparam
		if resp, ok := TaskResponseMap[statusparam]; ok {
			statuses[resp.Status] = 1
		} else if n, err := strconv.Atoi(statusparam); err == nil && n >= 0 && n <= 4 {
			statuses[n] = 1
		}
	}

	pattern := q.Get("search")
	pageparams["search"] = pattern
	if pattern == "" {
		SendHTTPResponse(w, "Search parameter required", http.StatusInternalServerError)
		return
	} else {
		pattern = CheckPattern(pattern)
	}
	parts := strings.Split(pattern, " ")
	wheres := []string{}
	values := []interface{}{}
	for _, word := range parts {
		if word != "" {
			if resp, ok := TaskResponseMap[word]; ok {
				statuses[resp.Status] = 1
			} else {
				if !strings.Contains(word, "%") {
					word = "%" + word + "%"
				}
				wheres = append(wheres, "(Fqdn LIKE ? OR Alias LIKE ?)")
				values = append(values, word, word)
			}
		}
	}

	data := SearchResponse{
		HTMLayout: HTMLayout{Title: fmt.Sprintf("Поиск: %s", pattern), Base: "search", Pattern: pattern},
		Page:      page,
	}

	sources := []*SourceTable{}
	if err := m.Keeper.DB.Table("sources").Where(strings.Join(wheres, " AND "), values...).Find(&sources).Error; err != nil {
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	data.SourcesFound = len(sources)

	if data.SourcesFound > 0 {
		wheres = []string{}
		values = []interface{}{}
		in_id := make([]string, 0, data.SourcesFound)
		for _, source := range sources {
			source.ExecTimeout = keeper.TimeoutShow(source.TimeoutExec)
			source.SyncTimeout = keeper.TimeoutShow(source.TimeoutSync)
			in_id = append(in_id, "?")
			values = append(values, source.ID)
		}
		wheres = append(wheres, fmt.Sprintf("source_id IN (%s)", strings.Join(in_id, ",")))
		if len(statuses) > 0 {
			in_id = make([]string, 0, len(statuses))
			for status, _ := range statuses {
				in_id = append(in_id, "?")
				values = append(values, status)
			}
			wheres = append(wheres, fmt.Sprintf("tasks.status IN (%s)", strings.Join(in_id, ",")))
		}

		log.Printf("Select from tasks where %s, values: %v", strings.Join(wheres, " AND "), wheres)
		tasks := []*TaskTable{}
		if err := m.Keeper.DB.Table("tasks").Select("tasks.ID, tasks.ctime, tasks.mtime, tasks.source_id, tasks.start, tasks.finish, sources.alias, sources.fqdn, tasks.status, tasks.server_dir, tasks.client_dir, tasks.message").Joins("join sources on tasks.source_id = sources.id").Where(strings.Join(wheres, " AND "), values...).Order("tasks.ctime desc").Limit(RowLimit).Offset((page - 1) * RowLimit).Find(&tasks).Error; err != nil {
			SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
			return
		}

		var taskCount int
		if err := m.Keeper.DB.Table("tasks").Joins("join sources on tasks.source_id = sources.id").Where(strings.Join(wheres, " AND "), values...).Count(&taskCount).Error; err != nil {
			SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
			return
		}

		for _, task := range tasks {
			task.StartStr = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", task.Start.Year(), task.Start.Month(), task.Start.Day(), task.Start.Hour(), task.Start.Minute(), task.Start.Second())
			task.FinishStr = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", task.Finish.Year(), task.Finish.Month(), task.Finish.Day(), task.Finish.Hour(), task.Finish.Minute(), task.Finish.Second())
			task.StatusName = TaskStatusMap[task.Status].Name
			log.Printf("id=%d, from %s to %s", task.ID, task.StartStr, task.FinishStr)
		}
		data.Tasks = tasks
		data.TasksRead = len(tasks)
		data.TasksFound = taskCount
	}
	data.Header = "Поиск: " + data.Pattern
	data.Sources = sources

	if data.TasksRead < data.TasksFound {
		data.UsePaginator = true
		data.Paginator = Paginator(&PaginatorArgs{Total: data.TasksFound, PageSize: RowLimit, PageNum: page, Base: "/search", Params: pageparams})
	}

	t := m.Template.Lookup("search")
	if t != nil {
		for _, check := range t.Templates() {
			log.Println(check.Name())
		}
		t.Execute(w, data)
	} else {
		SendHTTPResponse(w, "Template 'sources' not found", http.StatusInternalServerError)
	}
}

func CheckPattern(in string) string {
	return strings.ToLower(strings.Replace(in, "*", "%", -1))
}
