package monitor

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"gokkun/keeper"
)

type TaskTable struct {
	keeper.Model
	Fqdn       string
	Alias      string
	Start      time.Time
	StartStr   string
	Finish     time.Time
	FinishStr  string
	Status     int
	StatusName string
	SourceID   uint   `gorm:"column:source_id"`
	ServerDir  string `gorm:"column:server_dir"`
	ClientDir  string `gorm:"column:client_dir"`
	Message    string
	BaseURL    string
}

type SingleTaskResponse struct {
	HTMLayout
	Task   *TaskTable
	Source *SourceTable
}

type TasksResponse struct {
	HTMLayout
	Status       int
	Tasks        []*TaskTable
	RowsTotal    int
	RowsRead     int
	Page         int
	UsePaginator bool
	Paginator    []*Href
}

type TaskStatus struct {
	Id          int
	Name, Alias string
}

var TaskResponseMap = map[string]TasksResponse{
	"failed":    TasksResponse{HTMLayout: HTMLayout{Title: "Зафейленные задачи", Header: "Задачи в статусе failed"}, Status: 0},
	"executing": TasksResponse{HTMLayout: HTMLayout{Title: "Локальный бекап", Header: "Локальный бекап"}, Status: 1},
	"scheduled": TasksResponse{HTMLayout: HTMLayout{Title: "Готово к копированию", Header: "Готовые к копированию задачи"}, Status: 2},
	"copying":   TasksResponse{HTMLayout: HTMLayout{Title: "Копирование", Header: "Копируются"}, Status: 3},
	"success":   TasksResponse{HTMLayout: HTMLayout{Title: "Завершено", Header: "Завершено"}, Status: 4},
}

var TaskStatusMap = map[int]TaskStatus{
	0: TaskStatus{Id: 0, Name: "Error", Alias: "failed"},
	1: TaskStatus{Id: 1, Name: "Local backup", Alias: "executing"},
	2: TaskStatus{Id: 2, Name: "Ready to copy", Alias: "scheduled"},
	3: TaskStatus{Id: 3, Name: "Copying", Alias: "copying"},
	4: TaskStatus{Id: 4, Name: "Finished", Alias: "success"},
}

func (m *Mon) Task(w http.ResponseWriter, r *http.Request, id int64) {
	data := SingleTaskResponse{
		HTMLayout: HTMLayout{Title: fmt.Sprintf("Задание: id=%d", id), Base: "task"},
	}

	task := TaskTable{}
	if err := m.Keeper.DB.Table("tasks").Select("tasks.ID, tasks.ctime, tasks.mtime, tasks.source_id, tasks.start, tasks.finish, sources.alias, sources.fqdn, tasks.status, tasks.server_dir, tasks.client_dir, tasks.message").Joins("join sources on tasks.source_id = sources.id").First(&task, id).Error; err != nil {
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if task.ID > 0 {
		task.StartStr = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", task.Start.Year(), task.Start.Month(), task.Start.Day(), task.Start.Hour(), task.Start.Minute(), task.Start.Second())
		task.FinishStr = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", task.Finish.Year(), task.Finish.Month(), task.Finish.Day(), task.Finish.Hour(), task.Finish.Minute(), task.Finish.Second())
		task.StatusName = TaskStatusMap[task.Status].Name
		task.BaseURL = "/" + TaskStatusMap[task.Status].Alias
		serverDir := m.Config.Server.BaseDir
		if len(serverDir) > 0 && serverDir[len(serverDir)-1:] == "/" {
			task.ServerDir = serverDir[:len(serverDir)-1] + task.ServerDir
		} else {
			task.ServerDir = serverDir + task.ServerDir
		}
		clientDir := m.Config.Client.BaseDir
		if len(serverDir) > 0 && clientDir[len(clientDir)-1:] == "/" {
			task.ClientDir = clientDir[:len(clientDir)-1] + task.ClientDir
		} else {
			task.ClientDir = clientDir + task.ClientDir
		}
	} else {
		SendHTTPResponse(w, "Task not found", http.StatusNotFound)
		return
	}
	data.Task = &task
	data.Header = fmt.Sprintf("Источник: %s", task.Fqdn)

	t := m.Template.Lookup("task")
	if t != nil {
		for _, check := range t.Templates() {
			log.Println(check.Name())
		}
		t.Execute(w, data)
	} else {
		SendHTTPResponse(w, "Template 'task' not found", http.StatusInternalServerError)
	}
}

func (m *Mon) Tasks(w http.ResponseWriter, r *http.Request, base string) {
	q := r.URL.Query()
	pagestr := q.Get("page")
	page := 1
	if pagestr != "" {
		if i, err := strconv.Atoi(pagestr); err != nil {
			SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
			return
		} else {
			page = i
		}
	}

	data := TasksResponse{
		HTMLayout: HTMLayout{Title: TaskResponseMap[base].Title, Header: TaskResponseMap[base].Header, Base: base},
		Status:    TaskResponseMap[base].Status,
		Page:      page,
	}

	tasks := []*TaskTable{}
	if err := m.Keeper.DB.Table("tasks").Select("tasks.ID, tasks.ctime, tasks.mtime, tasks.source_id, tasks.start, tasks.finish, sources.alias, sources.fqdn, tasks.status, tasks.server_dir, tasks.client_dir, tasks.message").Joins("join sources on tasks.source_id = sources.id").Where("tasks.status = ?", data.Status).Order("tasks.ctime desc").Limit(RowLimit).Offset((page - 1) * RowLimit).Find(&tasks).Error; err != nil {
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var taskCount int
	if err := m.Keeper.DB.Table("tasks").Joins("join sources on tasks.source_id = sources.id").Where("tasks.status = ?", data.Status).Count(&taskCount).Error; err != nil {
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for _, task := range tasks {
		task.StartStr = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", task.Start.Year(), task.Start.Month(), task.Start.Day(), task.Start.Hour(), task.Start.Minute(), task.Start.Second())
		task.FinishStr = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", task.Finish.Year(), task.Finish.Month(), task.Finish.Day(), task.Finish.Hour(), task.Finish.Minute(), task.Finish.Second())
		task.StatusName = TaskStatusMap[task.Status].Name
	}
	data.Tasks = tasks
	data.RowsRead = len(tasks)
	data.RowsTotal = taskCount

	if data.RowsRead < data.RowsTotal {
		data.UsePaginator = true
		data.Paginator = Paginator(&PaginatorArgs{Total: data.RowsTotal, PageSize: RowLimit, PageNum: page, Base: "/" + base})
	}

	t := m.Template.Lookup("tasks")
	if t != nil {
		for _, check := range t.Templates() {
			log.Println(check.Name())
		}
		t.Execute(w, data)
	} else {
		SendHTTPResponse(w, "Template 'home' not found", http.StatusInternalServerError)
	}
}
