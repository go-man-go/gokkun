package monitor

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"
	"strconv"
	"time"
	"text/template"

	"gokkun/client"
	"gokkun/config"
	"gokkun/keeper"
)

type SourceTable struct {
	keeper.Source
	ExecTimeout string
	SyncTimeout string
}

type SourcesResponse struct {
	HTMLayout
	Sources   []*SourceTable
	RowsTotal int
	RowsRead  int
}

type SourceResponse struct {
	HTMLayout
	Source       *SourceTable
	Tasks        []*TaskTable
	RowsTotal    int
	RowsRead     int
	Page         int
	UsePaginator bool
	Paginator    []*Href
}

func (m *Mon) Source(w http.ResponseWriter, r *http.Request, id int64) {
	q := r.URL.Query()
	pagestr := q.Get("page")
	page := 1
	if pagestr != "" {
		if i, err := strconv.Atoi(pagestr); err != nil {
			SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
			return
		} else {
			page = i
		}
	}

	data := SourceResponse{
		HTMLayout: HTMLayout{Title: fmt.Sprintf("Источник: id=%d", id), Base: "source"},
		Page:      page,
	}

	source := SourceTable{}
	if err := m.Keeper.DB.Table("sources").First(&source, id).Error; err != nil {
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if source.ID > 0 {
		source.ExecTimeout = keeper.TimeoutShow(source.TimeoutExec)
		source.SyncTimeout = keeper.TimeoutShow(source.TimeoutSync)
		data.Source = &source
	} else {
		SendHTTPResponse(w, "Source not found", http.StatusNotFound)
		return
	}

	tasks := []*TaskTable{}
	if err := m.Keeper.DB.Table("tasks").Select("tasks.ID, tasks.ctime, tasks.mtime, tasks.source_id, tasks.start, tasks.finish, sources.alias, sources.fqdn, tasks.status, tasks.server_dir, tasks.client_dir, tasks.message").Joins("join sources on tasks.source_id = sources.id").Where("tasks.source_id = ?", id).Order("tasks.ctime desc").Limit(RowLimit).Offset((page - 1) * RowLimit).Find(&tasks).Error; err != nil {
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var taskCount int
	if err := m.Keeper.DB.Table("tasks").Joins("join sources on tasks.source_id = sources.id").Where("tasks.source_id = ?", id).Count(&taskCount).Error; err != nil {
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for _, task := range tasks {
		task.StartStr = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", task.Start.Year(), task.Start.Month(), task.Start.Day(), task.Start.Hour(), task.Start.Minute(), task.Start.Second())
		task.FinishStr = fmt.Sprintf("%04d-%02d-%02d %02d:%02d:%02d", task.Finish.Year(), task.Finish.Month(), task.Finish.Day(), task.Finish.Hour(), task.Finish.Minute(), task.Finish.Second())
		task.StatusName = TaskStatusMap[task.Status].Name
		log.Printf("id=%d, from %s to %s", task.ID, task.StartStr, task.FinishStr)
	}
	data.Tasks = tasks
	data.Header = "Источник: " + source.Fqdn
	data.RowsRead = len(tasks)
	data.RowsTotal = taskCount

	if data.RowsRead < data.RowsTotal {
		data.UsePaginator = true
		data.Paginator = Paginator(&PaginatorArgs{Total: data.RowsTotal, PageSize: RowLimit, PageNum: page, Base: "/sources/" + strconv.Itoa(int(id))})
	}

	t := m.Template.Lookup("source")
	if t != nil {
		for _, check := range t.Templates() {
			log.Println(check.Name())
		}
		t.Execute(w, data)
	} else {
		SendHTTPResponse(w, "Template 'source' not found", http.StatusInternalServerError)
	}
}

func (m *Mon) Sources(w http.ResponseWriter, r *http.Request) {
	data := SourcesResponse{
		HTMLayout: HTMLayout{Title: "Источники", Header: "Список источников", Base: "sources"},
	}
	log.Printf("Tasks using data: %v\n", data)

	sources := []*SourceTable{}
	if err := m.Keeper.DB.Table("sources").Order("fqdn").Find(&sources).Error; err != nil {
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for _, src := range sources {
		src.ExecTimeout = keeper.TimeoutShow(src.TimeoutExec)
		src.SyncTimeout = keeper.TimeoutShow(src.TimeoutSync)
	}
	data.Sources = sources
	data.RowsRead = len(sources)

	t := m.Template.Lookup("sources")
	if t != nil {
		for _, check := range t.Templates() {
			log.Println(check.Name())
		}
		t.Execute(w, data)
	} else {
		SendHTTPResponse(w, "Template 'sources' not found", http.StatusInternalServerError)
	}
}

func (m *Mon) GenScript(w http.ResponseWriter, r *http.Request, id int64) {
	Now := time.Now()
	api_call := fmt.Sprintf("http://%s:%s/source/%d", m.Config.Api.Host, m.Config.Api.Port, id)
	log.Printf("[GenScript] API call: %s", api_call)

	resp, err := config.HttpGet(api_call)
	if err != nil {
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	log.Printf("%s", body)
	Source := keeper.Source{}
	if err := json.Unmarshal(body, &Source); err != nil {
		log.Printf("[GenScript] Unmarshal response error %v", err)
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	resp.Body.Close()

	SourceEnv := keeper.ChefEnv{}
	err = json.Unmarshal([]byte(Source.Env), &SourceEnv)
	if err != nil {
		log.Printf("[GenScript] Json unmarshal Env (%s) error %v", Source.Env, err)
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	baseDir := m.Config.Client.BaseDir
	if baseDir == "" {
		baseDir = `[insert basedir here]`
	}

	Client := &client.Client{
		Template: template.Must(template.ParseGlob(filepath.Join(m.Config.Client.TemplateRoot, "gokkun-face", "scripts", "*"))),
	}
	Task := client.Task{
		Source: Source,
		Env:    SourceEnv,
		Client: Client,
	}
	Task.PartPath = fmt.Sprintf("%s-%d/%04d_%02d_%02d__%02d_%02d-XXXXX", Source.Alias, Source.ID, Now.Year(), Now.Month(), Now.Day(), Now.Hour(), Now.Minute())
	StorePath := fmt.Sprintf("%s/%s", baseDir, Task.PartPath)

	pageData, err := Task.CreateTaskScriptFromTemplate(StorePath)
	if err != nil {
		log.Printf("[GenScript] Generator error %v", err)
		SendHTTPResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	SendTextResponse(w, pageData)
}
