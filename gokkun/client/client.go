package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/gorhill/cronexpr"

	"gokkun/api"
	"gokkun/config"
	"gokkun/keeper"
)

const (
	JOB_INIT         = 0
	JOB_PROC         = 1
	JOB_REPORT_OK    = 2
	JOB_REPORT_FAULT = 3
	JOB_CLEAN        = 4
	JOB_FINISHED     = 5
)

type Client struct {
	BaseDir      string
	ApiHost      string
	FQDN         string
	FqdnId       string
	Template     *template.Template
	RegetTimeout time.Duration
	NextGet      time.Time
}

type Task struct {
	Client        *Client
	Source        keeper.Source
	Env           keeper.ChefEnv
	TaskID        uint
	Stage         int
	PartPath      string
	PartPathParam string
	ErrorMessage  string
	NextTry       time.Time
}

func New(c *config.Conf, fqdn string) *Client {
	client := Client{
		BaseDir:      c.Client.BaseDir,
		ApiHost:      "http://" + c.Api.Host + ":" + c.Api.Port,
		FQDN:         fqdn,
		NextGet:      time.Now(),
		RegetTimeout: time.Minute * 10,
	}
	client.FqdnId = strings.Replace(fqdn, ".", "|", -1)
	if _, err := os.Stat(c.Client.BaseDir); os.IsNotExist(err) {
		// Create if not exists
		if err = os.MkdirAll(c.Client.BaseDir, 0755); err != nil {
			log.Fatalf("[Client] Can't create dir %s", c.Client.BaseDir)
			config.CryAndDie("Client", fmt.Sprintf("Can't create dir %s", c.Client.BaseDir))
		}
	}
	client.Template = template.Must(template.ParseGlob(filepath.Join(c.Client.TemplateRoot, "gokkun-face", "scripts", "*")))

	return &client
}

func (c *Client) Start() {
	api_call := fmt.Sprintf("%s/fqdn/%s", c.ApiHost, c.FqdnId)

	Tasks := map[uint]Task{}
	ProcFlag := 1
	for ProcFlag > 0 {
		Now := time.Now()

		if Now.After(c.NextGet) {
			c.NextGet = c.NextGet.Add(c.RegetTimeout)

			resp, err := config.HttpGet(api_call)
			if err != nil {
				log.Printf("[Client] Get json through API %s error %v", api_call, err)
				continue
			}
			body, err := ioutil.ReadAll(resp.Body)
			log.Printf("%s", body)
			Sources := []keeper.Source{}
			if err := json.Unmarshal(body, &Sources); err != nil {
				log.Printf("[Client] Unmarshal response error %v", err)
				continue
			}
			resp.Body.Close()
			if len(Sources) == 0 {
				log.Printf("[Client] host %s is not registered in DB. Sleeping for an hour", c.FQDN)
				c.RegetTimeout = time.Hour
				if len(Tasks) == 0 {
					continue
				}
			} else {
				c.RegetTimeout = time.Minute * 10
			}
			for _, Source := range Sources {
				if _, exists := Tasks[Source.ID]; !exists {
					Job := Task{
						Client: c,
						Source: Source,
						Stage:  JOB_INIT,
					}
					SourceEnv := keeper.ChefEnv{}
					err := json.Unmarshal([]byte(Source.Env), &SourceEnv)
					if err != nil {
						log.Printf("[Client] Json unmarshal Env (%s) error %v", Source.Env, err)
						continue
					}
					Job.Env = SourceEnv
					Job.NextTry = Job.GetNextTime(Now)
					log.Printf("[Client] set task [source_id=%d] proc at %s", Job.Source.ID, Job.NextTry)
					Tasks[Source.ID] = Job
				} else {
					// ALTERNATE WORKFLOW:
					// А вот здесь нужно переинициализировать источник у задания.
				}
			}
			// ALTERNATE WORKFLOW:
			// Вот тут следует чистить удаленные из БД источники
		}
		if Tasks != nil && len(Tasks) > 0 {
			ToDelete := []uint{}
			for Source_ID, Job := range Tasks {
				Job.Process()
				if Job.Stage == JOB_FINISHED {
					ToDelete = append(ToDelete, Source_ID)
				}
			}
			// ALTERNATE WORKFLOW:
			// Для целей большей устойчивости код ниже можно удалить,
			// но обязательно дописать процедуру проверки и чистки
			// выброшенных или замененных в БД источников
			if len(ToDelete) > 0 {
				for _, Source_ID := range ToDelete {
					delete(Tasks, Source_ID)
				}
			}
		}
		time.Sleep(time.Minute)
	}

}

func (t *Task) Process() {
	Now := time.Now()
	Source := t.Source

	if t.Stage == JOB_INIT || t.Stage == JOB_FINISHED {
		if Now.After(t.NextTry) || Now.Equal(t.NextTry) {
			t.Stage = JOB_INIT
			t.NextTry = t.GetNextTime(Now)
		} else {
			return
		}
	}
	if t.Stage == JOB_INIT {
		if config.DEBUG {
			log.Printf("Processing task for source=[%d]: %v", t.Source.ID, t)
		}
		// Register new job
		api_call := fmt.Sprintf("%s/job/%d/start/%s", t.Client.ApiHost, Source.ID, t.Client.FqdnId)

		resp, err := config.HttpGet(api_call)
		if err != nil {
			log.Printf("[Client] Get json through API %s error %v", api_call, err)
			return
		}
		body, err := ioutil.ReadAll(resp.Body)
		StartCode := api.OperationResult{}
		if err := json.Unmarshal(body, &StartCode); err != nil {
			log.Printf("[Client] Unmarshal StartCode error %v", err)
			return
		}
		if StartCode.Result == "Error" {
			log.Printf("[Client] Api error: %s", StartCode.Description)
			return
		}
		resp.Body.Close()
		t.TaskID = StartCode.ResultID
		t.PartPath = fmt.Sprintf("%s-%d/%04d_%02d_%02d__%02d_%02d-%d", Source.Alias, Source.ID, Now.Year(), Now.Month(), Now.Day(), Now.Hour(), Now.Minute(), t.TaskID)
		t.PartPathParam = fmt.Sprintf("%s-%d|%04d_%02d_%02d__%02d_%02d-%d", Source.Alias, Source.ID, Now.Year(), Now.Month(), Now.Day(), Now.Hour(), Now.Minute(), t.TaskID)
		t.Stage = JOB_PROC
	}

	// Prepare backup dirs and script
	StorePath := fmt.Sprintf("%s/%s", t.Client.BaseDir, t.PartPath)
	ScriptPath := fmt.Sprintf("%s/gokkun.sh", StorePath)

	if t.Stage == JOB_PROC {
		log.Printf("[Client]: script path %s", ScriptPath)
		if _, err := os.Stat(StorePath); os.IsNotExist(err) {
			// Create if not exists
			log.Printf("[Client]: create path %s", StorePath)
			err = os.MkdirAll(StorePath, 0755)
			if err != nil {
				config.CryAndDie("Client", fmt.Sprintf("Can't create backup path %s: %v", StorePath, err))
			}
		}
		ScriptData, err := t.CreateTaskScriptFromTemplate(StorePath)
		if err := ioutil.WriteFile(ScriptPath, []byte(ScriptData), 0755); err != nil {
			config.CryAndDie("Client", fmt.Sprintf("Can't create script file %s: %v", ScriptPath, err))
		}

		if config.DEBUG {
			log.Printf("[Client] Backup process started...")
		}
		// Backup data
		BackupResult, err := t.Execute(Source.TimeoutExec, ScriptPath)
		if err != nil {
			t.ErrorMessage = fmt.Sprintf("[Client] %s: %v", BackupResult, err)
			log.Print(t.ErrorMessage)
			// Нужен Fallback
			t.FallBack(StorePath)
			t.ReportFault(t.ErrorMessage)
			return
		}
		if config.DEBUG {
			log.Printf("[Client] Backup process ended")
		}
		t.Stage = JOB_REPORT_OK
	}

	if t.Stage == JOB_REPORT_OK {
		// Register job end
		api_call := fmt.Sprintf("%s/job/%d/finish/%s", t.Client.ApiHost, t.TaskID, t.PartPathParam)
		if config.DEBUG {
			log.Printf("Api finish: %s", api_call)
		}

		resp, err := config.HttpGet(api_call)
		if err != nil {
			log.Printf("[Client] Get json through API %s error %v", api_call, err)
			return
		}
		body, err := ioutil.ReadAll(resp.Body)
		EndCode := api.OperationResult{}
		if err := json.Unmarshal(body, &EndCode); err != nil {
			log.Printf("[Client] Unmarshal EndCode error %v", err)
			return
		}
		if EndCode.Result == "Error" {
			log.Printf("[Client] Api error: %s", EndCode.Description)
			return
		}
		resp.Body.Close()
		t.Stage = JOB_CLEAN
	}

	if t.Stage == JOB_REPORT_FAULT {
		t.ReportFault(t.ErrorMessage)
	}

	if t.Stage == JOB_CLEAN {
		// Delete old backups
		if config.DEBUG {
			log.Printf("[Client] Stage JOB_CLEAN: Keep %d sources", Source.KeepRemote)
		}
		BackupRoot := fmt.Sprintf("%s/%s-%d/", t.Client.BaseDir, Source.Alias, Source.ID)
		if config.DEBUG {
			log.Printf("[Client] Stage JOB_CLEAN: BackupRoot %s", BackupRoot)
		}
		Backups, err := ioutil.ReadDir(BackupRoot)
		if err != nil {
			config.CryAndDie("Client", fmt.Sprintf("Can't get dir listing: %v", err))
		}
		if config.DEBUG {
			log.Printf("[Client] Dirs found: %v", Backups)
		}
		for _, f := range Backups[:len(Backups)-Source.KeepRemote] {
			if config.DEBUG {
				log.Printf("Remove %s%s", BackupRoot, f.Name())
			}
			if err := t.RemoveArchive(BackupRoot, f.Name()); err != nil {
				return
			}
		}
		t.Stage = JOB_FINISHED
	}
}

func (t *Task) Execute(timeout int, command string, args ...string) (string, error) {

	// instantiate new command
	cmd := exec.Command(command, args...)

	// get pipe to standard output
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return "cmd.StdoutPipe() error: " + err.Error(), err
	}

	// start process via command
	if err := cmd.Start(); err != nil {
		return "cmd.Start() error: " + err.Error(), err
	}

	// setup a buffer to capture standard output
	var buf bytes.Buffer

	// create a channel to capture any errors from wait
	done := make(chan error)
	go func() {
		if _, err := buf.ReadFrom(stdout); err != nil {
			panic("buf.Read(stdout) error: " + err.Error())
		}
		done <- cmd.Wait()
	}()

	// block on select, and switch based on actions received
	select {
	case <-time.After(time.Duration(timeout) * time.Minute):
		if err := cmd.Process.Kill(); err != nil {
			return "failed to kill: " + err.Error(), err
		}
		return "timeout reached, process killed", errors.New("Execute timeout")
	case err := <-done:
		if err != nil {
			close(done)
			buf.ReadFrom(stdout)
			return fmt.Sprintf("process done, with error: %v. Stdout:\n%v", err.Error(), buf.String()), err
		}
		return "process completed: " + buf.String(), nil
	}
	return "", nil
}

func (t *Task) FallBack(StorePath string) {
	if t.Env.DeleteFailed {
		log.Printf("[Client] Fallback: delete task %d data from %s", t.TaskID, StorePath)
		os.RemoveAll(StorePath)
	} else {
		log.Printf("[Client] Fallback: enviromental 'failed_tasks_delete' param prohibit deletion of task %d data from %s", t.TaskID, StorePath)
	}
}

func (t *Task) RemoveArchive(BackupRoot, DirName string) error {
	var JobID uint
	if n := strings.Index(DirName, "-"); n > 0 {
		if job_id, err := strconv.Atoi(string(DirName[0:n])); err != nil {
			log.Printf("[Client] DirName (%s) containing wrong TaskID. Deleting...", DirName)
		} else {
			JobID = uint(job_id)
			if t.TaskID == JobID {
				return nil
			}

			api_call := fmt.Sprintf("%s/job/%d", t.Client.ApiHost, JobID)

			resp, err := config.HttpGet(api_call)
			if err != nil {
				ErrMsg := fmt.Sprintf("[Client] Get json through API %s error %v", api_call, err)
				log.Printf(ErrMsg)
				err := errors.New(ErrMsg)
				return err
			}
			body, err := ioutil.ReadAll(resp.Body)
			resp.Body.Close()

			Job := keeper.Task{}
			if err := json.Unmarshal(body, &Job); err != nil {
				ErrMsg := fmt.Sprintf("[Client] Get task unmarshal error %v", err)
				log.Printf(ErrMsg)
				err := errors.New(ErrMsg)
				return err
			}

			if Job.ID > 0 && Job.Status > 0 && Job.Status < 4 {
				log.Printf("[Client] task %v is in use. Exiting", Job)
				return nil
			}
		}
	} else {
		log.Printf("Wrong DirName (%s) not containing TaskID. Deleting...", DirName)
	}
	os.RemoveAll(BackupRoot + DirName)
	return nil
}

func (t *Task) ReportFault(Message string) {
	t.Stage = JOB_REPORT_FAULT

	// Report to Riemann
	msg := config.NewMsg()
	event := config.Event{
		Service:     "Gokkun client: " + t.Client.FQDN,
		State:       "critical",
		Description: Message,
	}
	msg.AddEvent(event.ToRiemannEvent())
	config.Riemann.Send(msg)

	// Report job fault to API
	api_call := fmt.Sprintf("%s/job/%d/fail", t.Client.ApiHost, t.TaskID)
	log.Printf("Api fail report: %s", api_call)
	report := api.FaultReport{
		Id:        t.TaskID,
		Message:   Message,
		EventTime: time.Now(),
	}
	ReportBody, _ := json.Marshal(report)

	resp, err := config.HttpPost(api_call, "application/json", bytes.NewBuffer(ReportBody))
	if err != nil {
		log.Printf("[Client] Post json through API %s error %v", api_call, err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	FaultCode := api.OperationResult{}
	if err := json.Unmarshal(body, &FaultCode); err != nil {
		log.Printf("[Client] Unmarshal FaultCode error %v", err)
		return
	}
	if FaultCode.Result == "Error" {
		log.Printf("[Client] Api error: %s", FaultCode.Description)
		return
	}
	t.Stage = JOB_FINISHED
}

type FileTemplate struct {
	TarOptions                       string
	StorePath, SourceDir, BackupFile string
}

type ScriptTemplate struct {
	Type       string
	TarOptions string
	DBOptions  string
	StorePath  string
	Databases  string
	DoCompress string
	Jobs       []FileTemplate
}

func (t *Task) CreateTaskScriptFromTemplate(StorePath string) (string, error) {
	Source := t.Source
	stpl := ScriptTemplate{Type: Source.Alias, StorePath: StorePath, TarOptions: t.Env.TarOptions}
	if t.Env.DontCompress {
		stpl.DoCompress = "no"
	} else {
		stpl.DoCompress = "yes"
	}

	if Source.Alias == "files" {
		stpl.Jobs = make([]FileTemplate, 0, len(t.Env.Dirs))
		for _, SourceDir := range t.Env.Dirs {
			job := FileTemplate{TarOptions: stpl.TarOptions, SourceDir: SourceDir, StorePath: stpl.StorePath}
			job.BackupFile = strings.TrimLeft(strings.Replace(SourceDir, "/", "_", -1), "_")
			stpl.Jobs = append(stpl.Jobs, job)
		}
	} else if Source.Alias == "postgres" {
		stpl.DBOptions = t.Env.DBOptions
		if len(t.Env.Databases) > 0 {
			stpl.Databases = fmt.Sprintf(`"%s"`, strings.Join(t.Env.Databases, " "))
		} else {
			stpl.Databases = `$(psql -U postgres -qtc 'SELECT datname FROM pg_database WHERE datistemplate = false;')`
		}
	} else if Source.Alias == "mysql" {
		stpl.DBOptions = t.Env.DBOptions
		if len(t.Env.Databases) > 0 {
			stpl.Databases = fmt.Sprintf(`"%s"`, strings.Join(t.Env.Databases, " "))
		} else {
			stpl.Databases = `$(echo "show databases;" | ${MYSQL} | tail -n+2)`
		}
	} else if Source.Alias == "mongo" {
		stpl.DBOptions = t.Env.DBOptions
		if len(t.Env.Databases) > 0 {
			stpl.Databases = fmt.Sprintf(`"%s"`, strings.Join(t.Env.Databases, " "))
		} else {
			stpl.Databases = `$(echo "db.getMongo().getDBNames()"|mongo --quiet |tr -d \[\]\"" " | tr , " ")`
		}
	} else if Source.Alias == "tarantool" {
		stpl.DBOptions = t.Env.DBOptions
	} else {
		err := errors.New("Unknown job type: " + Source.Alias)
		log.Printf("[Client] %v", err)
		return "", err
	}

	var buf bytes.Buffer
	w := io.Writer(&buf)
	template := t.Client.Template.Lookup(Source.Alias)
	if template != nil {
		template.Execute(w, stpl)
	} else {
		return "", fmt.Errorf("Template '%s' not found", Source.Alias)
	}

	return buf.String(), nil
}

func (t *Task) CreateTaskScript(StorePath string) (string, error) {
	Source := t.Source
	ScriptData := ScriptPrefix

	if Source.Alias == "files" {
		for _, SourceDir := range t.Env.Dirs {
			BackupFile := strings.TrimLeft(strings.Replace(SourceDir, "/", "_", -1), "_")
			CompressExt, CompressTube := ".bz2", "| pbzip2 -2lc"
			if t.Env.DontCompress {
				CompressExt, CompressTube = "", ""
			}
			ScriptData += fmt.Sprintf("tar  --create --one-file-system --preserve-permissions --recursion --sparse --totals --wildcards --dereference %s --file - %s %s > %s/%s.tar%s\n", t.Env.TarOptions, SourceDir, CompressTube, StorePath, BackupFile, CompressExt)
		}
	} else if Source.Alias == "postgres" {
		ScriptData += fmt.Sprintf("\nCONFIG_DEFAULT_OPTIONS=\" -Fc\"\n")
		ScriptData += fmt.Sprintf("CONFIG_OPTIONS=\" ${CONFIG_EXTRA_OPTIONS}\"\n")

		if len(t.Env.Databases) > 0 {
			ScriptData += fmt.Sprintf("\nDATABASES=\"%s\"\n", strings.Join(t.Env.Databases, " "))
		} else {
			ScriptData += fmt.Sprintf("\nDATABASES=$(psql -U postgres -qtc 'SELECT datname FROM pg_database WHERE datistemplate = false;')\n")
		}
		CompressExt, CompressTube := ".bz2", "| pbzip2 -lc"
		if t.Env.DontCompress {
			CompressExt, CompressTube = "", ""
		}
		ScriptData += fmt.Sprintf("for DB in $DATABASES; do\n")
		ScriptData += fmt.Sprintf("pg_dump -U postgres $DB $CONFIG_OPTIONS %s > %s/${DB}.dump%s;\n", CompressTube, StorePath, CompressExt)
		ScriptData += fmt.Sprintf("done\n")
	} else if Source.Alias == "mysql" {
		ScriptData += fmt.Sprintf("DB_USER=\"root\"\n\n")
		ScriptData += fmt.Sprintf("BZIP2=\"pbzip2 -l\"\n")
		ScriptData += fmt.Sprintf("MYSQL=\"mysql -u ${DB_USER}\"\n\n")
		if len(t.Env.Databases) > 0 {
			ScriptData += fmt.Sprintf("\nDATABASES=\"%s\"\n", strings.Join(t.Env.Databases, " "))
		} else {
			ScriptData += fmt.Sprintf("\nDATABASES=$(echo \"show databases;\" | ${MYSQL} | tail -n+2)\n")
		}
		CompressExt, CompressTube := ".bz2", "| ${BZIP2} -c"
		if t.Env.DontCompress {
			CompressExt, CompressTube = "", ""
		}
		ScriptData += fmt.Sprintf("for DB_NAME in ${DATABASES}; do\n")
		ScriptData += fmt.Sprintf("mysqldump --max_allowed_packet=512M --quick --single-transaction -R -u ${DB_USER} ${DB_NAME} %s > %s/${DB_NAME}.sql%s;\n", CompressTube, StorePath, CompressExt)
		ScriptData += fmt.Sprintf("done\n")
	} else {
		err := errors.New("Unknown job type: " + Source.Alias)
		log.Printf("[Client] %v", err)
		return "", err
	}
	return ScriptData, nil
}

func (t *Task) GetNextTime(Now time.Time) time.Time {
	if t.Source.Cron != "" {
		nextTime := cronexpr.MustParse(t.Source.Cron).Next(Now)
		if !nextTime.IsZero() {
			return nextTime
		}
	}
	return Now.Add(time.Hour * 2)
}
