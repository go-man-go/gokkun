package client

var ScriptPrefix string = `#!/bin/bash

set -e
set -o pipefail

ionice -c 3 -p $$
chrt -p -i 0 $$

function logger() {

local log_level=$1
local message=$2
local log_date=$(date '+%Y-%m-%d-%H:%M:%S')

echo -e \"$log_date : $log_level : [$stage] : $message\"
}

function error() {
local err=$?
current_dir=$(pwd -P)

logger "ERROR" "************************************"
logger "ERROR" "Info:"
logger "ERROR" " * Current dir : $current_dir"
logger "ERROR" " * Command     : $BASH_COMMAND"
logger "ERROR" " * Error       : $err"
logger "ERROR" " * Line        : $BASH_LINENO"
logger "ERROR" "************************************"
logger "ERROR" "Exit"

exit 1
}

trap error ERR
`