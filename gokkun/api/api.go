package api

import (
	"github.com/ant0ine/go-json-rest/rest"
	"net/http"
	"log"
	"time"

	"gokkun/config"
	"gokkun/keeper"
)

type Api struct {
	Keeper	*keeper.Keeper
	Config	*config.Conf
}

func NewApi ( k *keeper.Keeper, c *config.Conf ) {
	a := Api{
		Keeper: k,
		Config: c,
	}
	
	api := rest.NewApi()
	api.Use(rest.DefaultDevStack...)
	router, err := rest.MakeRouter(
		rest.Get("/", a.StartPage),
		rest.Get("/server", a.GetAllServers),
		rest.Get("/server/get/:fqdn", a.GetServer),
		rest.Get("/server/:id/get-available-task", a.ServerGetAvailableTask),
		rest.Get("/server/:id/get-expired/month", a.ServerGetExpiredTasks),
		rest.Get("/fqdn", a.GetAllClients),
		rest.Get("/fqdn/:fqdn", a.GetClient),
		rest.Get("/source", a.GetAllSources),
		rest.Get("/source/:id", a.GetSource),
		rest.Get("/job/:id/start/:fqdn", a.ClientTaskStart),
		rest.Get("/job/:id/finish/:path", a.ClientTaskFinish),
		rest.Post("/job/:id/fail", a.ClientTaskFail),
		rest.Get("/job/:id/stored", a.StoreTaskFinish),
		rest.Post("/job/:id/store-fail", a.StoreTaskFail),
		rest.Get("/job/:id", a.GetTask),
		rest.Delete("/job/:id", a.DeleteTask),
	)
	if err != nil {
		log.Fatal("API server error: %v\n", err)
	}
	api.SetApp(router)
	log.Fatal("API server error: %v\n", http.ListenAndServe(":" + c.Api.Port, api.MakeHandler()))
}

type Hello struct {
	Message		string	`json:"message"`
}

type OperationResult struct {
	Result		string	`json:"result"`
	ResultID	uint	`json:"id"`
	Description	string	`json:"description"`
}

type FaultReport struct {
	Id        uint      `json:"id"`
	Message   string    `json:"message"`
	EventTime time.Time `json:"etime"`
}

func (a *Api) StartPage ( w rest.ResponseWriter, r *rest.Request ) {
	data := Hello{ Message: "Hello there!" }
	w.WriteJson(&data)
}

func (a *Api) GetAllClients ( w rest.ResponseWriter, r *rest.Request ) {
	data := Hello{ Message: "Hello there!" }
	w.WriteJson(&data)
}

func (a *Api) GetAllServers ( w rest.ResponseWriter, r *rest.Request ) {
	data := Hello{ Message: "Hello there!" }
	w.WriteJson(&data)
}

func (a *Api) GetAllSources ( w rest.ResponseWriter, r *rest.Request ) {
	data := Hello{ Message: "Hello there!" }
	w.WriteJson(&data)
}
