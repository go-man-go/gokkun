package api

import (
	"github.com/ant0ine/go-json-rest/rest"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	"gokkun/config"
	"gokkun/keeper"
)

var (
	mu sync.Mutex
)

func (a *Api) GetServer(w rest.ResponseWriter, r *rest.Request) {
	fqdn, err := url.PathUnescape(r.PathParam("fqdn"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fqdn = strings.Replace(fqdn, "|", ".", -1)
	if config.DEBUG {
		log.Printf("[%s] GetServer FQDN: %s\n", r.RemoteAddr, fqdn)
	}
	server := []keeper.Server{}
	if a.Keeper.DB.Where(&keeper.Server{Fqdn: fqdn, Status: 1}).Find(&server).Error != nil {
		rest.NotFound(w, r)
		return
	}
	w.WriteJson(&server)
}

func (a *Api) ServerGetAvailableTask(w rest.ResponseWriter, r *rest.Request) {
	task := keeper.Task{}

	server_param, err := url.PathUnescape(r.PathParam("id"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	server_id, err := strconv.Atoi(server_param)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	server := keeper.Server{}
	a.Keeper.DB.First(&server, server_id)
	if server.ID == 0 {
		rest.NotFound(w, r)
		return
	} else {
		err := a.serverRegisterTask(server.ID, &task, r)
		if err != nil {
			rest.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	w.WriteJson(&task)
}

func (a *Api) ServerGetExpiredTasks(w rest.ResponseWriter, r *rest.Request) {
	Now := time.Now()
	Today := Now.Truncate(24 * time.Hour)
	var thisMonth time.Time
	if Now.Day() > 1 {
		thisMonth = Today.AddDate(0, 0, -(Now.Day() - 1))
	} else {
		thisMonth = Today
	}
	prevMonth := thisMonth.AddDate(0, -1, 0)

	var startInterval time.Time
	if prevMonth.Weekday() > 1 {
		startInterval = prevMonth.AddDate(0, 0, 1-int(prevMonth.Weekday()))
	} else {
		startInterval = prevMonth
	}

	server_param, err := url.PathUnescape(r.PathParam("id"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	server_id, err := strconv.Atoi(server_param)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	server := keeper.Server{}
	a.Keeper.DB.First(&server, server_id)
	if server.ID == 0 {
		rest.NotFound(w, r)
		return
	}

	if config.DEBUG {
		log.Printf("[%s] ServerGetExpiredTasks: search tasks in interval: %v to %v", r.RemoteAddr, startInterval, thisMonth)
	}
	SourceIDs := []keeper.Task{}
	if err := a.Keeper.DB.Select("DISTINCT source_id").Where("master_id = ? AND status = 4 AND finish BETWEEN ? AND ?", server.ID, startInterval, thisMonth).Find(&SourceIDs).Error; err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	Result := []keeper.Task{}

	for _, SourceDistinct := range SourceIDs {
		if config.DEBUG {
			log.Printf("[%s] ServerGetExpiredTasks: get tasks for SourceID = %d", r.RemoteAddr, SourceDistinct.SourceID)
		}
		tasks := []keeper.Task{}
		if err := a.Keeper.DB.Where("master_id = ? AND source_id = ? AND status = 4 AND finish BETWEEN ? AND ?", server.ID, SourceDistinct.SourceID, startInterval, thisMonth).Order("finish").Find(&tasks).Error; err != nil {
			rest.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if config.DEBUG {
			log.Printf("[%s] ServerGetExpiredTasks: found tasks: ready to copy: %d", r.RemoteAddr, len(tasks))
		}
		localStart := startInterval
		localEnd := localStart.AddDate(0, 0, 7)
		if localEnd.After(thisMonth) {
			localEnd = thisMonth
		}
		index := 0
		for localStart.Before(thisMonth) {
			if config.DEBUG {
				log.Printf("[%s] ServerGetExpiredTasks: search tasks in interval: %v to %v", r.RemoteAddr, localStart, localEnd)
			}
			Alive := map[uint]*keeper.Task{}
			for len(tasks) > index && localEnd.After(tasks[index].Finish) {
				if Task, ok := Alive[tasks[index].SourceID]; ok {
					Result = append(Result, *Task)
				}
				Alive[tasks[index].SourceID] = &tasks[index]
				index++
			}
			localStart = localEnd
			localEnd = localStart.AddDate(0, 0, 7)
			if localEnd.After(thisMonth) {
				localEnd = thisMonth
			}
		}
	}
	w.WriteJson(&Result)
}

func (a *Api) serverRegisterTask(server_id uint, task *keeper.Task, r *rest.Request) error {
	mu.Lock()
	defer mu.Unlock()

	tx := a.Keeper.DB.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	if err := tx.Error; err != nil {
		return err
	}
	tx.Where(&keeper.Task{MasterID: uint(server_id), Status: 2}).First(task)
	if task.ID != 0 {
		if config.DEBUG {
			log.Printf("[%s] serverRegisterTask: found task ready to copy. Task id = %d", r.RemoteAddr, task.ID)
		}
		task.Start = time.Now()
		task.Status = 3
		err := tx.Save(task).Error
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	err := tx.Commit().Error
	if err != nil {
		tx.Rollback()
		return err
	}
	return nil
}
