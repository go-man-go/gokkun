package api

import (
	"fmt"
	"github.com/ant0ine/go-json-rest/rest"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"gokkun/keeper"
)

func (a *Api) GetClient(w rest.ResponseWriter, r *rest.Request) {
	fqdn, err := url.PathUnescape(r.PathParam("fqdn"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fqdn = strings.Replace(fqdn, "|", ".", -1)
	log.Printf("[%s] GetClient FQDN: %s\n", r.RemoteAddr, fqdn)
	source := []keeper.Source{}
	if a.Keeper.DB.Where(&keeper.Source{Fqdn: fqdn, Status: 1}).Find(&source).Error != nil {
		rest.NotFound(w, r)
		return
	}
	w.WriteJson(&source)
}

func (a *Api) GetSource(w rest.ResponseWriter, r *rest.Request) {
	source_param, err := url.PathUnescape(r.PathParam("id"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	source_id, err := strconv.Atoi(source_param)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Printf("[%s] GetSource with id=%d\n", r.RemoteAddr, source_id)
	source := keeper.Source{}
	if a.Keeper.DB.First(&source, source_id).Error != nil {
		rest.NotFound(w, r)
		return
	}
	w.WriteJson(&source)
}

func (a *Api) GetTask(w rest.ResponseWriter, r *rest.Request) {
	task_param, err := url.PathUnescape(r.PathParam("id"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	task_id, err := strconv.Atoi(task_param)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Printf("[%s] GetTask with id=%d\n", r.RemoteAddr, task_id)
	task := keeper.Task{}
	if a.Keeper.DB.First(&task, task_id).Error != nil {
		rest.NotFound(w, r)
		return
	}
	w.WriteJson(&task)
}

func (a *Api) DeleteTask(w rest.ResponseWriter, r *rest.Request) {
	response := OperationResult{}
	task_param, err := url.PathUnescape(r.PathParam("id"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	task_id, err := strconv.Atoi(task_param)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	log.Printf("[%s] DeleteTask with id=%d\n", r.RemoteAddr, task_id)
	task := keeper.Task{}
	task.ID = uint(task_id)
	if err := a.Keeper.DB.Delete(&task).Error; err != nil {
		response.Result = "Error"
		response.ResultID = uint(task_id)
		response.Description = fmt.Sprintf("%v", err)
	} else {
		response.Result = "OK"
		response.ResultID = uint(task_id)
	}
	w.WriteJson(&response)
}

func (a *Api) ClientTaskStart(w rest.ResponseWriter, r *rest.Request) {
	response := OperationResult{}
	fqdn, err := url.PathUnescape(r.PathParam("fqdn"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fqdn = strings.Replace(fqdn, "|", ".", -1)

	source_param, err := url.PathUnescape(r.PathParam("id"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	source_id, err := strconv.Atoi(source_param)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	log.Printf("[%s] ClientTaskStart: get source with id=%d\n", r.RemoteAddr, source_id)
	source := keeper.Source{}
	a.Keeper.DB.First(&source, source_id)
	if source.ID == 0 {
		response.Result = "Error"
		response.Description = "Source (id=" + source_param + ") not found"
	} else {
		task := keeper.Task{}
		log.Printf("[%s] ClientTaskStart: get task with source_id=%d\n", r.RemoteAddr, source_id)
		a.Keeper.DB.Where(&keeper.Task{SourceID: uint(source_id), Status: 1}).First(&task)
		if task.ID != 0 {
			log.Printf("[%s] ClientTaskStart: found unfinished task for fqdn %s, task id = %d", r.RemoteAddr, fqdn, task.ID)
			task.Start = time.Now()
			if err := a.Keeper.DB.Save(&task).Error; err != nil {
				rest.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		} else {
			task = keeper.Task{
				Start:    time.Now(),
				Status:   1,
				SourceID: source.ID,
				MasterID: source.MasterID,
			}
			if err := a.Keeper.DB.Save(&task).Error; err != nil {
				rest.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		}
		response.Result = "OK"
		response.ResultID = task.ID
	}

	w.WriteJson(&response)
}

func (a *Api) ClientTaskFail(w rest.ResponseWriter, r *rest.Request) {
	response := OperationResult{}
	report := FaultReport{}
	if err := r.DecodeJsonPayload(&report); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if report.Id > 0 {
		task := keeper.Task{}
		log.Printf("[%s] ClientTaskFail: get task with id=%d\n", r.RemoteAddr, report.Id)
		if a.Keeper.DB.Where("status = 1").First(&task, report.Id).Error != nil {
			response.Result = "Error"
			response.Description = fmt.Sprintf("Opened task with id=%d not found", report.Id)
		} else {
			task.Status = 0
			task.Finish = report.EventTime
			task.Message = report.Message

			if err := a.Keeper.DB.Save(&task).Error; err != nil {
				rest.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			response.Result = "OK"
			response.ResultID = task.ID
		}
	} else {
		response.Result = "Error"
		response.Description = "Bad task id"
	}
	w.WriteJson(&response)
}

func (a *Api) ClientTaskFinish(w rest.ResponseWriter, r *rest.Request) {
	Now := time.Now()

	response := OperationResult{}
	LocalPath, err := url.PathUnescape(r.PathParam("path"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	LocalPath = strings.Replace(LocalPath, "|", "/", -1)

	task_param, err := url.PathUnescape(r.PathParam("id"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	task_id, err := strconv.Atoi(task_param)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if task_id > 0 {
		task := keeper.Task{}
		log.Printf("[%s] ClientTaskFinish: get task with id=%d\n", r.RemoteAddr, task_id)
		if a.Keeper.DB.Where("status = 1").First(&task, task_id).Error != nil {
			response.Result = "Error"
			response.Description = fmt.Sprintf("Opened task with id=%d not found", task_id)
		} else {
			source := keeper.Source{}
			a.Keeper.DB.First(&source, task.SourceID)
			if source.ID == 0 {
				response.Result = "Error"
				response.Description = fmt.Sprintf("Source (id=%d) not found", task.SourceID)
			} else {
				task.Status = 2
				task.ClientDir = fmt.Sprintf("/%s", LocalPath)

				PathSlice := strings.Split(source.Fqdn, ".")
				PathBackSlice := []string{}
				for i := len(PathSlice) - 1; i >= 0; i = i - 1 {
					PathBackSlice = append(PathBackSlice, PathSlice[i])
				}
				PathPart := strings.Join(PathBackSlice, "/")
				task.ServerDir = fmt.Sprintf("/%s/%s", PathPart, LocalPath)
				task.Finish = Now

				if err := a.Keeper.DB.Save(&task).Error; err != nil {
					rest.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}
				response.Result = "OK"
				response.ResultID = task.ID
			}
		}
	} else {
		response.Result = "Error"
		response.Description = "Bad task id"
	}
	w.WriteJson(&response)
}

func (a *Api) StoreTaskFail(w rest.ResponseWriter, r *rest.Request) {
	response := OperationResult{}
	report := FaultReport{}
	if err := r.DecodeJsonPayload(&report); err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if report.Id > 0 {
		task := keeper.Task{}
		log.Printf("[%s] StoreTaskFail: get task with id=%d\n", r.RemoteAddr, report.Id)
		if a.Keeper.DB.Where("status = 3").First(&task, report.Id).Error != nil {
			response.Result = "Error"
			response.Description = fmt.Sprintf("Opened task with id=%d not found", report.Id)
		} else {
			task.Status = 0
			task.Finish = report.EventTime
			task.Message = report.Message

			if err := a.Keeper.DB.Save(&task).Error; err != nil {
				rest.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			response.Result = "OK"
			response.ResultID = task.ID
		}
	} else {
		response.Result = "Error"
		response.Description = "Bad task id"
	}
	w.WriteJson(&response)
}

func (a *Api) StoreTaskFinish(w rest.ResponseWriter, r *rest.Request) {
	Now := time.Now()

	response := OperationResult{}

	task_param, err := url.PathUnescape(r.PathParam("id"))
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	task_id, err := strconv.Atoi(task_param)
	if err != nil {
		rest.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if task_id > 0 {
		task := keeper.Task{}
		log.Printf("[%s] StoreTaskFinish: get task with id=%d\n", r.RemoteAddr, task_id)
		if a.Keeper.DB.Where("status = 3").First(&task, task_id).Error != nil {
			response.Result = "Error"
			response.Description = fmt.Sprintf("Opened task with id=%d not found", task_id)
		} else {
			task.Status = 4
			task.Finish = Now

			if err := a.Keeper.DB.Save(&task).Error; err != nil {
				rest.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			response.Result = "OK"
			response.ResultID = task.ID
		}
	} else {
		response.Result = "Error"
		response.Description = "Bad task id"
	}
	w.WriteJson(&response)
}
