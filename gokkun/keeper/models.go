package keeper

import (
	"time"
//	"gokkun/config"
//	"github.com/jinzhu/gorm"
//	_ "github.com/jinzhu/gorm/dialects/postgres"
)

const (
	TASK_STATUS_ERROR   = 0
	TASK_STATUS_BACKUP  = 1
	TASK_STATUS_READY   = 2
	TASK_STATUS_COPY    = 3
	TASK_STATUS_FINISH  = 4
)

var TimeoutMap = map[int]string{
	30: "30 min",
	1:  "1 hour",
	2:  "2 hours",
	3:  "3 hours",
	4:  "4 hours",
	6:  "6 hours",
	8:  "8 hours",
}

type Model struct {
	ID          uint       `gorm:"primary_key"`
	CreatedAt   time.Time  `gorm:"column:ctime"`
	UpdatedAt   time.Time  `gorm:"column:mtime"`
}

type Server struct {
	Model
	Status      int
	Fqdn        string
}

type ChefEnv struct {
	IP              string      `json:"ip"`
	Dirs            []string    `json:"dirs"`
	Databases       []string    `json:"db"`
	TarOptions      string      `json:"tar_options"`
	DBOptions       string      `json:"db_options"`
	DontCompress    bool        `json:"no_compress"`
	ReserveCopy     bool        `json:"reserv_copy"`
	DeleteFailed    bool        `json:"failed_tasks_delete"`
}

type Source struct {
	Model
	Luptime     time.Time  `gorm:"column:dtime"`
	Status      int
	Fqdn        string
	Alias		string
	MasterID	uint       `gorm:"column:master_id"`
	KeepRemote	int        `gorm:"column:keep_remote"`
	TimeoutExec	int        `gorm:"column:timeout_exec"`
	TimeoutSync	int        `gorm:"column:timeout_sync"`
	Cron        string
	Env         string
	Tags        string
}

type Task struct {
	Model
	Start		time.Time
	Finish		time.Time
	Status      int
	SourceID	uint       `gorm:"column:source_id"`
	MasterID	uint       `gorm:"column:master_id"`
	Pid			uint
	ServerDir	string     `gorm:"column:server_dir"`
	ClientDir	string     `gorm:"column:client_dir"`
	Message		string
}