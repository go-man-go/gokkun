package keeper

import (
	"fmt"
	"strings"

	"gokkun/config"
	//	"database/sql"
	//	_ "github.com/lib/pq"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

type Keeper struct {
	DBHost, DBName, DBUser, DBPort, DBPass string
	DB                                     *gorm.DB
}

func New(c *config.Conf) (*Keeper, error) {
	keeper := &Keeper{
		DBHost: c.Postgres.Host,
		DBName: c.Postgres.Database,
		DBUser: c.Postgres.User,
		DBPass: c.Postgres.Password,
	}
	if c.Postgres.Port == "" {
		keeper.DBPort = "5432"
	} else {
		keeper.DBPort = c.Postgres.Port
	}
	db, err := gorm.Open("postgres", "host="+keeper.DBHost+" user="+keeper.DBUser+" dbname="+keeper.DBName+" sslmode=disable password="+keeper.DBPass)
	if err == nil {
		keeper.DB = db
	}

	return keeper, err
}

func TimeoutShow(dbvalue int) string {
	var result string
	if dbvalue <= 0 {
	} else if res, exist := TimeoutMap[dbvalue]; exist {
		result = res
	} else {
		parts := []string{}
		hours := dbvalue / 60
		if hours > 0 {
			if ( hours == 1 ) {
				parts = append(parts, fmt.Sprintf("%d hour", hours))
			} else {
				parts = append(parts, fmt.Sprintf("%d hours", hours))
			}
		}
		minutes := dbvalue % 60
		if minutes > 0 {
			parts = append(parts, fmt.Sprintf("%d min", minutes))
		}
		result = strings.Join(parts, " ")
	}
	return result
}
