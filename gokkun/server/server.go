package server

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
	"time"

	"gokkun/api"
	"gokkun/config"
	"gokkun/keeper"
)

type Server struct {
	BaseDir            string
	RemoteBase         string
	ApiHost            string
	FqdnId             string
	JobNum             int
	ServerID           uint
	Instance           *keeper.Server
	lastMonthlyCleanup time.Time
	lastYearlyCleanup  time.Time
}

type ServiceJob struct {
	LocalDir  string
	RemoteDir string
	FQDN      string
	ProcessID string
}

func New(c *config.Conf, fqdn string) *Server {
	server := Server{
		BaseDir:    c.Server.BaseDir,
		JobNum:     c.Server.JobNum,
		RemoteBase: c.Client.BaseDir,
		ApiHost:    "http://" + c.Api.Host + ":" + c.Api.Port,
	}
	server.FqdnId = strings.Replace(fqdn, ".", "|", -1)
	if _, err := os.Stat(c.Server.BaseDir); os.IsNotExist(err) {
		// Create if not exists
		if err = os.MkdirAll(c.Server.BaseDir, 0755); err != nil {
			log.Fatalf("[Server] Can't create dir %s\n", c.Server.BaseDir)
		}
	}

	return &server
}

func (s *Server) Start() {
	api_call := fmt.Sprintf("%s/server/get/%s", s.ApiHost, s.FqdnId)

	resp, err := config.HttpGet(api_call)
	if err != nil {
		log.Fatalf("[Server] Get json through API %s error %v", api_call, err)
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if config.DEBUG {
		log.Printf("%s\n", body)
	}
	response := []keeper.Server{}
	if err := json.Unmarshal(body, &response); err != nil {
		log.Fatalf("[Server] Unmarshal response error %v", err)
		return
	}
	if len(response) == 1 {
		Instance := response[0]
		s.ServerID = Instance.ID
		s.Instance = &Instance
	} else {
		log.Fatalf("[Server] Host is not registered server %s\n")
		return
	}

	go s.Cleanup()

	limit := make(chan struct{}, s.JobNum)
	for config.CanExecute() {
		limit <- struct{}{}
		go func() {
			s.Process()
			time.Sleep(1 * time.Second)
			defer func() { <-limit }()
		}()
	}
}

func (s *Server) Process() {
	api_call := fmt.Sprintf("%s/server/%d/get-available-task", s.ApiHost, s.ServerID)

	resp, err := config.HttpGet(api_call)
	if err != nil {
		log.Printf("[Server] Get json through API %s error %v", api_call, err)
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	Task := keeper.Task{}
	if err := json.Unmarshal(body, &Task); err != nil {
		log.Printf("[Server] Get task unmarshal error %v", err)
		return
	}

	if Task.ID > 0 {
		if config.DEBUG {
			log.Printf("%s\n", body)
			log.Printf("[Server] Got task %v\n", Task)
		}
		// Start Rsync procedure
		api_call = fmt.Sprintf("%s/source/%d", s.ApiHost, Task.SourceID)
		resp2, err := config.HttpGet(api_call)
		if err != nil {
			Message := fmt.Sprintf("[Server] Get json through API %s error %v", api_call, err)
			// Зафейлить таск
			s.ReportFault(Message, Task.ID)
			log.Printf(Message)
			return
		}
		defer resp2.Body.Close()
		body, err = ioutil.ReadAll(resp2.Body)
		log.Printf("%s\n", body)
		Source := keeper.Source{}
		if err := json.Unmarshal(body, &Source); err != nil {
			Message := fmt.Sprintf("[Server] Get source unmarshal error %v from %s", err, body)
			// Зафейлить таск
			s.ReportFault(Message, Task.ID)
			log.Printf(Message)
			return
		}

		StorePath := fmt.Sprintf("%s%s/", s.BaseDir, Task.ServerDir)
		if config.DEBUG {
			log.Printf("[Server] Store path: %s\n", StorePath)
		}
		if _, err := os.Stat(StorePath); os.IsNotExist(err) {
			// Create if not exists
			if config.DEBUG {
				log.Printf("[Server] Creating dir: %s", StorePath)
			}
			err = os.MkdirAll(StorePath, 0755)
			if err != nil {
				Message := fmt.Sprintf("[Server] Can't create backup path %s: %v\n", StorePath, err)
				// Зафейлить таск
				s.ReportFault(Message, Task.ID)
				log.Printf(Message)
				return
			}
		}
		// Rsync data
		if config.DEBUG {
			log.Printf("[Server] Trying to sync [rsync -e \"ssh -o StrictHostKeyChecking=no\" -avz %s:%s%s/ %s]...", Source.Fqdn, s.RemoteBase, Task.ClientDir, StorePath)
		}
		ExecResult, err := s.Execute(Source.TimeoutSync, "rsync", "-e", "ssh -o StrictHostKeyChecking=no", "-avz", fmt.Sprintf("%s:%s%s/", Source.Fqdn, s.RemoteBase, Task.ClientDir), StorePath)
		if err != nil {
			Message := fmt.Sprintf("[Server] %s: %v", ExecResult, err)
			log.Print(Message)
			s.ReportFault(Message, Task.ID)
			os.RemoveAll(StorePath)
			return
		}

		// Register job end
		api_call = fmt.Sprintf("%s/job/%d/stored", s.ApiHost, Task.ID)
		if config.DEBUG {
			log.Printf("Api finish: %s\n", api_call)
		}

		resp3, err := config.HttpGet(api_call)
		if err != nil {
			log.Printf("[Server] Get json through API %s error %v", api_call, err)
			// Rollback backup here
			os.RemoveAll(StorePath)
			return
		}
		defer resp3.Body.Close()
		body, err = ioutil.ReadAll(resp3.Body)
		EndCode := api.OperationResult{}
		if err := json.Unmarshal(body, &EndCode); err != nil {
			log.Printf("[Server] Unmarshal EndCode error %v", err)
			// Rollback backup here
			return
		}
		if EndCode.Result == "Error" {
			log.Printf("[Server] Api error: %s\n", EndCode.Description)
			// Rollback backup here
			os.RemoveAll(StorePath)
			return
		}
	}
}

func (s *Server) Execute(timeout int, command string, args ...string) (string, error) {

	// instantiate new command
	cmd := exec.Command(command, args...)

	// get pipe to standard output
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return "cmd.StdoutPipe() error: " + err.Error(), err
	}

	// start process via command
	if err := cmd.Start(); err != nil {
		return "cmd.Start() error: " + err.Error(), err
	}

	// setup a buffer to capture standard output
	var buf bytes.Buffer

	// create a channel to capture any errors from wait
	done := make(chan error)
	go func() {
		if _, err := buf.ReadFrom(stdout); err != nil {
			panic("buf.Read(stdout) error: " + err.Error())
		}
		done <- cmd.Wait()
	}()

	// block on select, and switch based on actions received
	select {
	case <-time.After(time.Duration(timeout) * time.Hour):
		if err := cmd.Process.Kill(); err != nil {
			return "failed to kill: " + err.Error(), err
		}
		return "timeout reached, process killed", errors.New("Execute timeout")
	case err := <-done:
		if err != nil {
			close(done)
			buf.ReadFrom(stdout)
			return fmt.Sprintf("process done, with error: %v. Stdout:\n%v", err.Error(), buf.String()), err
		}
		return "process completed: " + buf.String(), nil
	}
	return "", nil
}

func (s *Server) ReportFault(Message string, TaskID uint) {
	// Report job fault
	api_call := fmt.Sprintf("%s/job/%d/store-fail", s.ApiHost, TaskID)
	if config.DEBUG {
		log.Printf("Api fail report: %s\n", api_call)
	}
	report := api.FaultReport{
		Id:        TaskID,
		Message:   Message,
		EventTime: time.Now(),
	}
	ReportBody, _ := json.Marshal(report)

	resp, err := config.HttpPost(api_call, "application/json", bytes.NewBuffer(ReportBody))
	if err != nil {
		log.Printf("[Server] Post json through API %s error %v", api_call, err)
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	FaultCode := api.OperationResult{}
	if err := json.Unmarshal(body, &FaultCode); err != nil {
		log.Printf("[Server] Unmarshal FaultCode error %v", err)
		return
	}
	if FaultCode.Result == "Error" {
		log.Printf("[Server] Api error: %s\n", FaultCode.Description)
		return
	}
	resp.Body.Close()
}

func (s *Server) Cleanup() {
	Now := time.Now()
	for config.CanExecute() {
		if s.lastMonthlyCleanup.Month() < Now.Month() && Now.Day() > 6 {
			if s.CleanupLastMonth() {
				s.lastMonthlyCleanup = Now
			}
		}
		if config.CanExecute() {
			time.Sleep(10 * time.Minute)
		}
	}
}

func (s *Server) CleanupLastMonth() bool {
	api_call := fmt.Sprintf("%s/server/%d/get-expired/month", s.ApiHost, s.ServerID)
	if config.DEBUG {
		log.Printf("[Cleanup] CleanupLastMonth api call: %s\n", api_call)
	}
	resp, err := config.HttpGet(api_call)
	if err != nil {
		log.Printf("[Cleanup] Get json through API %s error %v", api_call, err)
		return false
	}
	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	if config.DEBUG {
		log.Printf("[Cleanup] CleanupLastMonth result: %s", body)
	}

	response := []keeper.Task{}
	if err := json.Unmarshal(body, &response); err != nil {
		log.Printf("[Cleanup] Unmarshal response error %v", err)
		return false
	}
	Errors := 0
	if len(response) > 0 {
		for _, Task := range response {
			if config.CanExecute() {
				RemovePath := fmt.Sprintf("%s%s", s.BaseDir, Task.ServerDir)
				if err := os.RemoveAll(RemovePath); err != nil {
					Errors++
				} else if err := s.DeleteTask(Task.ID); err != nil {
					Errors++
				}
			} else {
				return false
			}
			time.Sleep(100 * time.Millisecond)
		}
	}
	if Errors > 0 {
		return false
	} else {
		return true
	}
}

func (s *Server) DeleteTask(TaskID uint) error {
	// Remove task from DB
	api_call := fmt.Sprintf("%s/job/%d", s.ApiHost, TaskID)
	if config.DEBUG {
		log.Printf("[Server] Api job delete request: %s\n", api_call)
	}
	resp, err := config.HttpDelete(api_call)
	defer resp.Body.Close()
	if err != nil {
		log.Printf("[Server] Get json through API %s error %v", api_call, err)
		return err
	}
	body, err := ioutil.ReadAll(resp.Body)
	FaultCode := api.OperationResult{}
	if err := json.Unmarshal(body, &FaultCode); err != nil {
		log.Printf("[Server] Unmarshal FaultCode error %v", err)
		return err
	}
	if FaultCode.Result == "Error" {
		log.Printf("[Server] Api error: %s\n", FaultCode.Description)
		errors.New(FaultCode.Description)
	}
	return nil
}
