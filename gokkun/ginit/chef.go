package ginit

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gokkun/config"
	"gokkun/keeper"
)

type ChefEnv struct {
	IP              string      `json:"ip"`
	Dirs            []string    `json:"dirs"`
	Databases       []string    `json:"db"`
	TarOptions      string      `json:"tar_options"`
	DontCompress    bool        `json:"no_compress"`
	ReserveCopy     bool        `json:"reserv_copy"`
	DeleteFailed    bool        `json:"failed_tasks_delete"`
}

type ChefHost struct {
	Fqdn      string           `json:"fqdn"`
	Cron      string           `json:"cron"`
	Job       string           `json:"job"`
	Env       map[string]interface{}    `json:"env"`
	Tags      []string         `json:"tags"`
}

type ChefFaceResponse []*ChefHost
const (
	TIMEOUT_DEFAULT = 120
)

func Start ( k *keeper.Keeper, c *config.Conf ) {
    ProcFlag := 1
    for ProcFlag > 0 {
		InitFromChef(k, c)
		log.Println("Sleeping for an hour...")
		time.Sleep(time.Hour)
	}
}

func InitFromChef( k *keeper.Keeper, c *config.Conf ) {
	Now := time.Now()
	log.Printf("[Chef]: Get data from %s", c.ChefFaceUrl)

	resp, err := http.Get(c.ChefFaceUrl)
	if err != nil {
		log.Fatalf("[Chef] Download url %s error %v", c.ChefFaceUrl, err)
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	var response ChefFaceResponse
	if err := json.Unmarshal(body, &response); err != nil {
		log.Fatalf("[Chef] Unmarshal response error %v", err)
		return
	}

	// Get all records from DB.sources
	Sources := []keeper.Source{}
	if k.DB.Find(&Sources).Error != nil {
		log.Fatalf("[Chef]: Can't read sources from DB")
		return
	}
	SourceMap := make(map[string]keeper.Source)
	for _, Source := range Sources {
		Key := Source.Fqdn + "-" + Source.Alias + "-" + Source.Cron
		SourceMap[Key] = Source
	}

	// Reinit every record
	for _, JSource := range response {
		Key := JSource.Fqdn + "-" + JSource.Job + "-" + JSource.Cron
		log.Printf("[Chef] source: %s:%s[%s]", JSource.Fqdn, JSource.Job, JSource.Cron)
		if Source, Ok := SourceMap[Key]; Ok {
			if len(JSource.Env) > 0 {
				ParseEnv( &Source, JSource.Env )
				if err := k.DB.Save( &Source ).Error; err != nil {
					log.Printf("[Chef error]: Can't update source %v. Error: %v", Source, err)
				}
			}
			delete(SourceMap, Key)
		} else {
			Source := keeper.Source{
				Luptime:    Now,
				Status:     1,
				Fqdn:       JSource.Fqdn,
				Alias:      JSource.Job,
				Cron:       JSource.Cron,
				MasterID:   1,
			}
			if len(JSource.Env) > 0 {
				ParseEnv( &Source, JSource.Env )
			}
			if err := k.DB.Create( &Source ).Error; err != nil {
				log.Printf("[Chef error]: Can't create source %v. Error: %v", Source, err)
			}
		}
	}
	if len( SourceMap ) > 0 {
		tx := k.DB.Begin()
		defer func() {
			if r := recover(); r != nil {
				tx.Rollback()
			}
		}()
		if tx.Error != nil {
			log.Fatalf("[Chef]: Can't start DB transaction for mass delete")
			return
		}
		for _, Source := range SourceMap {
			if err := tx.Delete(&Source).Error; err != nil {
				tx.Rollback()
				log.Fatalf("[Chef]: Can't delete source %v from DB in transaction")
				return
			}
		}
		if err := tx.Commit().Error; err != nil {
			log.Fatalf("[Chef]: Can't commit DB in transaction")
			return
		}
	}
}

func ParseEnv( Source *keeper.Source, Env map[string]interface{} ) {
	EnvData := ChefEnv{
		DontCompress: false,
		ReserveCopy:  true,
		DeleteFailed: true,
	}
	if val, ok := Env["timeout_exec"]; ok && val != nil {
		timeout := ParseTimeout( val.(string) )
		if timeout > 0 {
			Source.TimeoutExec = timeout
		} else {
			Source.TimeoutExec = TIMEOUT_DEFAULT
		}
		delete( Env, "timeout_exec" )
	} else {
		Source.TimeoutExec = TIMEOUT_DEFAULT
	}
	if val, ok := Env["timeout_sync"]; ok && val != nil {
		timeout := ParseTimeout( val.(string) )
		if timeout > 0 {
			Source.TimeoutSync = timeout
		} else {
			Source.TimeoutSync = TIMEOUT_DEFAULT
		}
		delete( Env, "timeout_sync" )
	} else {
		Source.TimeoutSync = TIMEOUT_DEFAULT
	}
	if val, ok := Env["keep_remote"]; ok && val != nil {
		Source.KeepRemote = int(val.(float64))
		if Source.KeepRemote == 0 {
			Source.KeepRemote = 1
		}
		delete( Env, "keep_remote" )
	} else {
		Source.KeepRemote = 1
	}
	if len( Env ) > 0 {
		for Key, Value := range Env {
			switch Val := Value.(type) {
				case []interface{}:
					result := []string{}
					for _, data := range Val {
						result = append(result, data.(string))
					}
					if Key == "dirs" {
						EnvData.Dirs = result
					} else if Key == "db" {
						EnvData.Databases = result
					}
				case string:
					if Key == "ip" {
						EnvData.IP = Val
					} else if Key == "tar_options" {
						EnvData.TarOptions = Val
					}
				case bool:
					if Key == "no_compress" {
						EnvData.DontCompress = Val
					} else if Key == "reserv_copy" {
						EnvData.ReserveCopy = Val
					} else if Key == "failed_tasks_delete" {
						EnvData.DeleteFailed = Val
					}
			}
		}
	}
	if JsonData, err := json.Marshal( EnvData ); err == nil {
		Source.Env = string( JsonData )
	}
}

func ParseTimeout ( T string ) int {
	if len(T) > 0 {
		var numstr, dimension string;
		for _, B := range T {
			chr := string(B)
			if strings.Contains("0123456789", chr) {
				numstr += chr
			} else if strings.Contains("minhour", chr) {
				dimension += chr
			}
		}
		if number, err := strconv.Atoi(numstr); err == nil {
			if strings.ToLower( string(dimension[0]) ) == "h" {
				return number * 60
			} else if strings.ToLower( string(dimension[0]) ) == "m" {
				return number
			} else {
				return 0
			}
		} else {
			return 0
		}
	}
	return 0
}